package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import main.java.com.getjavajob.training.algo08.lazareve.util.Matrix;
import java.util.HashMap;
import java.util.Map;

public class MatrixHashMap<V> implements Matrix<V> {

    private Map<Key, V> table;

    public MatrixHashMap() {
        table = new HashMap<>();
    }

    @Override
    public V get(int i, int j) {
        return table.get(new Key(i, j));
    }

    @Override
    public void set(int i, int j, V value) {
        table.put(new Key(i, j), value);
    }

    static class Key {

        private final int i;
        private final int j;

        Key(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (i != key.i) return false;
            return j == key.j;

        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            return result;
        }
    }
}
