package main.java.com.getjavajob.training.algo08.lazareve.lesson09;

import java.util.NavigableSet;
import java.util.TreeSet;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class NavigableSetTest {

    private static final NavigableSet<String> TREE_SET;
    private static final NavigableSet<String> EMPTY_SET;
    private static final String[] A;

    static {
        TREE_SET = new TreeSet<>();
        TREE_SET.add("A");
        TREE_SET.add("AB");
        TREE_SET.add("ABC");
        TREE_SET.add("B");
        TREE_SET.add("BA");
        TREE_SET.add("BAC");
        TREE_SET.add("C");
        TREE_SET.add("CA");
        TREE_SET.add("CAB");
        TREE_SET.add("CB");

        EMPTY_SET = new TreeSet<>();

        A = new String[1];
    }

    public static void main(String[] args) {
        lowerTest();
        floorTest();
        ceilingTest();
        higherTest();
        pollFirstTest();
        pollLastTest();
        descendingSetTest();
    }

    private static void lowerTest() {
        assertEquals("NavigableSet.lower()", "A", TREE_SET.lower("AB"));
        assertEquals("NavigableSet.lower()", "CB", TREE_SET.lower("blabla"));
        try {
            TREE_SET.lower(null);
            fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.lower()", true, e instanceof NullPointerException);
        }
    }

    private static void floorTest() {
        assertEquals("NavigableSet.floor()", "A", TREE_SET.floor("AAAA"));
        assertEquals("NavigableSet.floor()", "CB", TREE_SET.floor("blabla"));
        assertEquals("NavigableSet.floor()", true, EMPTY_SET.floor("blabla") == null);
        try {
            TREE_SET.floor(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableSet.floor()", true, e instanceof NullPointerException);
        }
    }

    private static void ceilingTest() {
        assertEquals("NavigableSet.ceiling()", "C", TREE_SET.ceiling("BACA"));
        assertEquals("NavigableSet.ceiling()", true, TREE_SET.ceiling("blabla") == null);
        assertEquals("NavigableSet.ceiling()", true, EMPTY_SET.ceiling("blabla") == null);
        try {
            TREE_SET.ceiling(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableSet.ceiling()", true, e instanceof NullPointerException);
        }
    }

    private static void higherTest() {
        assertEquals("NavigableSet.higher()", "CA", TREE_SET.higher("C"));
        assertEquals("NavigableSet.higher()", true, TREE_SET.higher("blabla") == null);
        assertEquals("NavigableSet.higher()", true, EMPTY_SET.higher("blabla") == null);
        try {
            TREE_SET.higher(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableSet.higher()", true, e instanceof NullPointerException);
        }
    }

    private static void pollFirstTest() {
        TreeSet<String> set = new TreeSet<>(TREE_SET);
        assertEquals("NavigableSet.pollFirst()", "A", set.pollFirst());
        assertEquals("NavigableSet.pollFirst()", TREE_SET.size() - 1, set.size());
        assertEquals("NavigableSet.pollFirst()", true, EMPTY_SET.pollFirst() == null);
    }

    private static void pollLastTest() {
        NavigableSet<String> set = new TreeSet<>(TREE_SET);
        assertEquals("NavigableSet.pollLast()", "CB", set.pollLast());
        assertEquals("NavigableSet.pollLast()", TREE_SET.size() - 1, set.size());
        assertEquals("NavigableSet.pollLast()", true, EMPTY_SET.pollLast() == null);
    }

    private static void descendingSetTest() {
        NavigableSet<String> set = new TreeSet<>(TREE_SET);
        NavigableSet<String> desSet = set.descendingSet();

        assertEquals("NavigableSet.descendingSet()", "CB", desSet.first());
        assertEquals("NavigableSet.descendingSet()", set.size(), desSet.size());
    }

}
