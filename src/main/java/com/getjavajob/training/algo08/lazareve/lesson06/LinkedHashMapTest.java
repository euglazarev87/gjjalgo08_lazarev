package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class LinkedHashMapTest {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        Map<String, String> set = new LinkedHashMap<>();
        set.put("First", "First");
        set.put("Second", "Second");
        set.put("Third", "Third");
        set.put("Fourth", "Fourth");
        set.put("Fifth", "Fifth");

        Iterator<String> iterator = set.keySet().iterator();
        assertEquals("LinkedHashMap.put()", "First", iterator.next());
        assertEquals("LinkedHashMap.put()", "Second", iterator.next());
        assertEquals("LinkedHashMap.put()", "Third", iterator.next());
        assertEquals("LinkedHashMap.put()", "Fourth", iterator.next());
        assertEquals("LinkedHashMap.put()", "Fifth", iterator.next());
    }
}
