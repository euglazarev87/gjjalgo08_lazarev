package main.java.com.getjavajob.training.algo08.lazareve.lesson09;

import main.java.com.getjavajob.training.algo08.lazareve.util.Assert;

import java.util.NoSuchElementException;
import java.util.SortedSet;
import java.util.TreeSet;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class SortedSetTest {

    private static final SortedSet<String> TREE_SET;
    private static String[] a;

    static {
        TREE_SET = new TreeSet<>();
        TREE_SET.add("A");
        TREE_SET.add("AB");
        TREE_SET.add("ABC");
        TREE_SET.add("B");
        TREE_SET.add("BA");
        TREE_SET.add("BAC");
        TREE_SET.add("C");
        TREE_SET.add("CA");
        TREE_SET.add("CAB");
        TREE_SET.add("CB");

        a = new String[1];
    }

    public static void main(String[] args) {
        subSetTest();
        headSetTest();
        tailSetTest();
        firstTest();
        lastTest();
    }

    private static void subSetTest() {

        String[] arr = TREE_SET.subSet("A", "AB").toArray(a);
        String[] test = {"A"};

        assertEquals("SortedSet.subSet()", test, arr);

        arr = TREE_SET.subSet("A", "crazystring").toArray(a);
        test = TREE_SET.toArray(new String[1]);
        assertEquals("SortedSet.subSet()", test, arr);

        arr = TREE_SET.subSet("crazy", "crazystring").toArray(a);
        test = new String[]{null};
        assertEquals("SortedSet.subSet()", test, arr);

        try {
            TREE_SET.subSet("a", "A");
            Assert.fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.subSet()", true, e instanceof IllegalArgumentException);
        }

        try {
            TREE_SET.subSet(null, "A");
            Assert.fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.subSet()", true, e instanceof NullPointerException);
        }
    }

    private static void headSetTest() {

        assertEquals("SortedSet.headSet()", new String[]{"A", "AB", "ABC"},
                TREE_SET.headSet("B").toArray(a));

        try {
            TREE_SET.headSet(null);
            fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.headSet()", true, e instanceof NullPointerException);
        }

        try {
            TREE_SET.headSet("b");
        } catch (Exception e) {
            assertEquals("SortedSet.headSet()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void tailSetTest() {

        assertEquals("SortedSet.tailSet()", new String[]{"C", "CA", "CAB", "CB"},
                TREE_SET.tailSet("C").toArray(a));

        try {
            TREE_SET.tailSet(null);
            fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.tailSet()", true, e instanceof NullPointerException);
        }

    }

    private static void firstTest() {
        assertEquals("SortedSet.first()", "A", TREE_SET.first());
        TreeSet<String> newSet = new TreeSet<>();
        try {
            newSet.first();
            fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.first()", true, e instanceof NoSuchElementException);
        }
    }

    private static void lastTest() {
        assertEquals("SortedSet.last()", "CB", TREE_SET.last());
        TreeSet<String> newSet = new TreeSet<>();
        try {
            newSet.last();
            fail();
        } catch (Throwable e) {
            assertEquals("SortedSet.last()", true, e instanceof NoSuchElementException);
        }
    }
}
