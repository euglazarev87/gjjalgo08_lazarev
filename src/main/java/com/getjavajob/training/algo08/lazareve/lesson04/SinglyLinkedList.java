package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import java.util.ArrayList;
import java.util.List;

class SinglyLinkedList<V> {

    protected Node<V> head;
    protected int size;

    public void add(V e) {
        if (head == null) {
            head = new Node<>(e);
        } else {
            Node<V> last = head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = new Node<>(e);
        }
        size++;
    }

    public V get(int index) throws IndexOutOfBoundsException {
        if (index >= size()) {
            throw new IndexOutOfBoundsException(""+index);
        }

        if (index == 0) {
            return head.val;
        }

        int i = 1;
        Node<V> node = head;
        while (i <= index) {
            node = node.next;
            i++;
        }
        return node.val;
    }

    public Node<V> get(V val) {
        Node<V> f = head;
        while (f != null) {
            if (val.equals(f.val)) {
                return f;
            }
            f = f.next;
        }
        return null;
    }

    public int size() {
        return size;
    }

    /**
     * O(n)*
     */
    public void reverse() {
        Object[] tail = new Object[size() / 2];
        Node<V> tailEl = head;
        for (int i = 0, j = 0; i < size(); i++) {
            if (i > size() / 2) {
                tail[j++] = tailEl;
            }
            tailEl = tailEl.next;
        }
        Node<V> one = head;
        for (int i = tail.length - 1; i >= 0; i--) {
            swap(one, (Node<V>)tail[i]);
            one = ((Node<V>)tail[i]).next;
        }
    }

    /**
     * Not optimal swap method. I don't know how to get previous
     * element in single linked list without traversal
     * @param one
     * @param two
     */
    protected void swap(Node<V> one, Node<V> two) {
        if (one == null || two == null) {
            return;
        }

        Node<V> it = head;
        Node<V> preOne = null;
        Node<V> preTwo = null;
        while (it.next != null) {
            if (it.next == one) {
                preOne = it;
            }

            if (it.next == two) {
                preTwo = it;
            }
            it = it.next;
        }

        Node<V> onesNext = one.next;
        Node<V> twosNext = two.next;
        if (one == head) {
            preTwo.next = one;
            one.next = two.next;
            two.next = onesNext;
            head = two;
            return;
        }

        if (two == head) {
            preOne.next = two;
            two.next = one.next;
            one.next = twosNext;
            head = one;
            return;
        }

        preTwo.next = one;
        one.next = twosNext;
        preOne.next = two;
        two.next = onesNext;
    }

    public List<V> asList() {
        List<V> list = new ArrayList<>();
        Node<V> f = head;
        while (f != null) {
            list.add(f.val);
            f = f.next;
        }
        return list;
    }

    private static class Node<V> {
        Node<V> next;
        V val;

        Node(V v) {
            val = v;
        }
    }
}
