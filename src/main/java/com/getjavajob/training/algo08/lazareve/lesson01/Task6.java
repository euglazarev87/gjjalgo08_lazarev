package main.java.com.getjavajob.training.algo08.lazareve.lesson01;

import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {
        System.out.println((int) (char) (byte) -1);
    }

    private static void solveAB() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter 2 numbers n and m (numbers <31 )");
        int n = sc.nextInt();
        int m = sc.nextInt();
        if (n > 30 || m > 30) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println(getXorToTwo(n));
        System.out.println(getXorToM(n, m));
    }

    private static void solveCDEFGH() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter number A and number N (N > 0)");
        int a = sc.nextInt();
        int n = sc.nextInt();
        if (n < 1) {
            System.out.println("Invalid input");
            return;
        }
        System.out.println(Integer.toBinaryString(a));
        System.out.println(Integer.toBinaryString(resetLowerBits(a, n)));
    }

    /**
     * given n<31, calc 2^n
     *
     * @param n
     * @return
     */
    public static int getXorToTwo(int n) {
        return 2 ^ n;
    }

    /**
     * given n,m<31, calc 2^n+2^m
     *
     * @param n
     * @param m
     * @return
     */
    public static int getXorToM(int n, int m) {
        return 2 ^ n + 2 ^ m;
    }

    /**
     * c) reset n lower bits (create mask with n lower bits reset)
     *
     * @param source
     * @param countOfBits
     * @return
     */
    public static int resetLowerBits(int source, int countOfBits) {
        int mask = -1 << countOfBits;
        return source & mask;
    }

    /**
     * set a's n-th bit with 1
     *
     * @param source
     * @param numberOfBit
     * @return
     */
    public static int setGivenBitWithOne(int source, int numberOfBit) {
        int mask = 1 << numberOfBit - 1;
        return source | mask;
    }

    /**
     * invert n-th bit (use 2 bit ops)
     *
     * @param source
     * @param numberOfBit
     * @return
     */
    public static int invertBit(int source, int numberOfBit) {
        return source ^ 1 << numberOfBit - 1;
    }

    /**
     * set a's n-th bit with 0
     *
     * @param source
     * @param numberOfBit
     * @return
     */
    public static int setGivenBitWithZero(int source, int numberOfBit) {
        return source & ~(1 << numberOfBit - 1);
    }

    /**
     * return n lower bits
     *
     * @param source
     * @param countOfBits
     * @return
     */
    public static int getLowerBits(int source, int countOfBits) {
        int mask = ~(-1 << countOfBits);
        return source & mask;
    }

    /**
     * return n-th bit
     *
     * @param source
     * @param numberOfBit
     * @return
     */
    public static boolean getNthBit(int source, int numberOfBit) {
        int mask = 1 << numberOfBit - 1;
        return (source & mask) >> numberOfBit - 1 == 1;
    }

    /**
     * given byte a. output bin representation using bit ops (don't use jdk api).
     *
     * @param source
     * @return
     */
    public static String getBinaryRepresentation(byte source) {
        char[] sb = new char[8];
        for (int i = 1, j = 7; i <= 8; i++, j--) {
            sb[j] = getNthBit(source, i) ? '\u0031' : '\u0030';
        }
        return new String(sb);
    }

    /**
     * swap 2 vars' values without intermediate var in 4 ways: 2 bitwise and 2 arithmetic solutions
     *
     * @param a
     * @param b
     */
    public static void swapVars(int a, int b) {
        // arihmetic
        a += b;
        b = a - b;
        a = a - b;

        a *= b;
        b = a / b;
        a = a / b;
        System.out.println(String.format("A = %s B = %s", a, b));

        // bitwise
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println(String.format("A = %s B = %s", a, b));
    }
}
