package main.java.com.getjavajob.training.algo08.lazareve.lesson09;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class SortedMapTest {

    private static final SortedMap<Integer, String> TREE_MAP;

    static {
        TREE_MAP = new TreeMap<>();
        TREE_MAP.put(1, "Alex");
        TREE_MAP.put(2, "Boris");
        TREE_MAP.put(3, "Calwin");
        TREE_MAP.put(4, "Denis");
        TREE_MAP.put(5, "Fedor");
        TREE_MAP.put(6, "Nikolai");
    }


    public static void main(String[] args) {
        subMapTest();
        headMapTest();
        tailMapTest();
        firstKeyTest();
        lastKeyTest();
        comparatorTest();
    }

    private static void comparatorTest() {
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return 0;
            }
        };

        SortedMap<Integer, String> map = new TreeMap<>(comp);
        assertEquals("SortedMap.comparator()", true, comp == map.comparator());
    }

    private static void subMapTest() {

        SortedMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        map.remove(6);
        assertEquals("SortedMap.subMap()", map, TREE_MAP.subMap(1, 6));
        assertEquals("SortedMap.subMap()", true, TREE_MAP.subMap(1, 1).isEmpty());
        assertEquals("SortedMap.subMap()", TREE_MAP, TREE_MAP.subMap(1, 10));

        try {
            assertEquals("SortedMap.subMap()", map, TREE_MAP.subMap(null, 1));
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.subMap()", true, e instanceof NullPointerException);
        }

        try {
            assertEquals("SortedMap.subMap()", map, TREE_MAP.subMap(2, 1));
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.subMap()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void headMapTest() {
        SortedMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        SortedMap<Integer, String> smap = new TreeMap<>();
        smap.put(1, "Alex");

        assertEquals("SortedMap.headMap()", smap, map.headMap(2));

        try {
            TREE_MAP.headMap(null);
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.headMap()", true, e instanceof NullPointerException);
        }
    }

    private static void tailMapTest() {
        SortedMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        SortedMap<Integer, String> smap = new TreeMap<>();
        smap.put(5, "Fedor");
        smap.put(6, "Nikolai");

        assertEquals("SortedMap.tailMap()", smap, map.tailMap(5));

        try {
            TREE_MAP.tailMap(null);
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.tailMap()", true, e instanceof NullPointerException);
        }
    }

    private static void firstKeyTest() {
        SortedMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("SortedMap.firstKey()", 1, map.firstKey());

        SortedMap<Integer, String> smap = new TreeMap<>();
        try {
            smap.firstKey();
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.firstKey()", true, e instanceof NoSuchElementException);
        }
    }

    private static void lastKeyTest() {
        SortedMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("SortedMap.lastKey()", 6, map.lastKey());

        SortedMap<Integer, String> smap = new TreeMap<>();
        try {
            smap.lastKey();
            fail();
        } catch (Throwable e) {
            assertEquals("SortedMap.lastKey()", true, e instanceof NoSuchElementException);
        }
    }
}
