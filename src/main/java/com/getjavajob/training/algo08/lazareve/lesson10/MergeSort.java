package main.java.com.getjavajob.training.algo08.lazareve.lesson10;

import static main.java.com.getjavajob.training.algo08.lazareve.util.MathHelper.safeAdd;
import static main.java.com.getjavajob.training.algo08.lazareve.util.MathHelper.safeDivide;

import java.util.Arrays;

public class MergeSort {

    public static <T extends Comparable<T>> void divided(T[] list, int begin, int end) {
        if (end - begin <= 1) {
            return;
        }

        int m = safeDivide(safeAdd(end, begin), 2);
        divided(list, begin, m);
        divided(list, m, end);
        merge(list, begin, m, m, end);
    }

    public static <T extends Comparable<T>> void sort(T[] list) {
        divided(list, 0, list.length);
    }

    private static <T extends Comparable<T>> void merge(T[] list, int leftBegin, int leftEnd, int rightBegin, int rightEnd) {
        T[] leftArr = Arrays.copyOfRange(list, leftBegin, leftEnd);
        T[] rightArr = Arrays.copyOfRange(list, rightBegin, rightEnd);

        int leftRemain = leftArr.length - 1;
        int rightRemain = rightArr.length - 1;
        int i = 0;
        int j = 0;
        int k = leftBegin;
        while (leftRemain >= 0 && rightRemain >= 0) {
            if (leftArr[i].compareTo(rightArr[j]) <= 0) {
                list[k] = leftArr[i++];
                leftRemain--;
            } else {
                list[k] = rightArr[j++];
                rightRemain--;
            }
            k++;
        }

        while (leftRemain >= 0) {
            list[k] = leftArr[i++];
            leftRemain--;
            k++;
        }
        while (rightRemain >= 0) {
            list[k] = rightArr[j++];
            rightRemain--;
            k++;
        }
    }
}