package main.java.com.getjavajob.training.algo08.lazareve.util;

import java.io.FileWriter;
import java.io.IOException;

public class Helper {

    public static void writeToFile(String fileName, String content) {
        try (FileWriter writer = new FileWriter(fileName, true)) {
            writer.append(content);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
