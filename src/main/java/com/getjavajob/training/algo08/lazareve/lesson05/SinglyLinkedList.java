package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import java.util.EmptyStackException;

class SinglyLinkedList<V> {

    protected Node<V> head;

    public SinglyLinkedList() {
    }

    public void reverse() {
        Node prev = null;
        Node current = head;
        Node next = null;
        while (current != null) {
            next = current.next;
            swap(prev, current);
            prev = current;
            current = next;
        }
        head = prev;
    }

    public void swap(Node<V> prev, Node<V> current) {
        current.next = prev;
    }

    public void push(V v) {
        if (head == null) {
            head = new Node<>(v);
        } else {
            head = new Node<>(v, head);
        }
    }

    public V pop() {
        if (head == null) {
            throw new EmptyStackException();
        }
        V val = head.val;
        head = head.next;
        return val;
    }

    public V removeLast() {
        if (head == null) {
            throw new EmptyStackException();
        }

        Node<V> lastNode = head;
        Node<V> prev = null;
        while (lastNode.next != null) {
            prev = lastNode;
            lastNode = lastNode.next;
        }
        if (prev != null) {
            prev.next = null;
        }

        if (lastNode == head) {
            head = null;
        }
        return lastNode.val;
    }

    static class Node<V> {
        Node<V> next;
        V val;

        public Node(V val) {
            this.val = val;
        }

        public Node(V val, Node<V> next) {
            this.val = val;
            this.next = next;
        }
    }
}
