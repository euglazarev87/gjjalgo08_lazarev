package main.java.com.getjavajob.training.algo08.lazareve.lesson03;

import main.java.com.getjavajob.training.algo08.lazareve.util.PerformanceTestResult;
import main.java.com.getjavajob.training.algo08.lazareve.util.StopWatch;

import java.util.ArrayList;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Helper.writeToFile;

public class DynamicArrayPerformanceTest {

    private static final int PERFORMANCE_TEST_SIZE = 10699567;
    private static PerformanceTestResult experimentalOne;
    private static PerformanceTestResult experimentalTwo;
    private static String resultFileName;

    public static void main(String[] args) {
        startTest();
    }

    private static void init() {
        experimentalOne = new PerformanceTestResult("DynamicArray");
        experimentalTwo = new PerformanceTestResult("ArrayList");
        resultFileName = "Lesson3DynamicArrayPerformanceTest";
    }

    private static void startTest() {
        init();
        StringBuilder sb = new StringBuilder();
        sb.append(beginningTest());
        sb.append(middleTest());
        sb.append(endTest());
        writeToFile(resultFileName, sb.toString());
        System.out.println("Performance test created");
    }

    private static String beginningTest() {
        DynamicArray dList = getDynamicArrayTemplate(PERFORMANCE_TEST_SIZE);
        ArrayList<Integer> aList = getArrayListTemplate(PERFORMANCE_TEST_SIZE);
        StopWatch sw = new StopWatch();

        sw.start();
        dList.add(0, 999);
        experimentalOne.addResult = sw.getElapsedTime();

        sw.start();
        aList.add(0, 999);
        experimentalTwo.addResult = sw.getElapsedTime();

        sw.start();
        dList.remove(0);
        experimentalOne.removeResult = sw.getElapsedTime();

        sw.start();
        aList.remove(0);
        experimentalTwo.removeResult = sw.getElapsedTime();

        return formatResult("Begin test");
    }

    private static String middleTest() {
        DynamicArray dList = getDynamicArrayTemplate(PERFORMANCE_TEST_SIZE);
        ArrayList<Integer> aList = getArrayListTemplate(PERFORMANCE_TEST_SIZE);
        StopWatch sw = new StopWatch();

        sw.start();
        dList.add(PERFORMANCE_TEST_SIZE / 2, 999);
        experimentalOne.addResult = sw.getElapsedTime();

        sw.start();
        aList.add(PERFORMANCE_TEST_SIZE / 2, 999);
        experimentalTwo.addResult = sw.getElapsedTime();

        sw.start();
        dList.remove(PERFORMANCE_TEST_SIZE / 2);
        experimentalOne.removeResult = sw.getElapsedTime();

        sw.start();
        aList.remove(PERFORMANCE_TEST_SIZE / 2);
        experimentalTwo.removeResult = sw.getElapsedTime();

        return formatResult("Middle test");
    }

    private static String endTest() {
        DynamicArray dList = getDynamicArrayTemplate(PERFORMANCE_TEST_SIZE);
        ArrayList<Integer> aList = getArrayListTemplate(PERFORMANCE_TEST_SIZE);
        StopWatch sw = new StopWatch();

        sw.start();
        dList.add(999);
        experimentalOne.addResult = sw.getElapsedTime();

        sw.start();
        aList.add(999);
        experimentalTwo.addResult = sw.getElapsedTime();

        sw.start();
        dList.remove(Integer.valueOf(PERFORMANCE_TEST_SIZE));
        experimentalOne.removeResult = sw.getElapsedTime();

        sw.start();
        aList.remove(Integer.valueOf(PERFORMANCE_TEST_SIZE));
        experimentalTwo.removeResult = sw.getElapsedTime();

        return formatResult("End test");
    }

    private static ArrayList<Integer> getArrayListTemplate(int length) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 1; i <= length; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    private static DynamicArray getDynamicArrayTemplate(int length) {
        DynamicArray dynamicList = new DynamicArray();
        for (int i = 1; i <= length; i++) {
            dynamicList.add(i);
        }
        return dynamicList;
    }

    private static String formatResult(String methodName) {

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(methodName + "\n");
        sb.append("==============================\n");
        sb.append(experimentalOne.className + ".add: " + experimentalOne.addResult + " ms\n");
        sb.append(experimentalTwo.className + ".add: " + experimentalTwo.addResult + " ms\n");
        sb.append("-----------\n");
        sb.append(experimentalOne.className + ".remove: " + experimentalOne.removeResult + " ms\n");
        sb.append(experimentalTwo.className + ".remove: " + experimentalTwo.removeResult + " ms\n");
        sb.append("==============================\n");

        return sb.toString();
    }
}
