package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import main.java.com.getjavajob.training.algo08.lazareve.util.ListPerformanceTest;

import java.util.LinkedList;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class DoublyLinkedListTest {

    public static void main(String[] args) {
        addTest();
        containsTest();
        indexOfTest();
        sizeTest();
        getTest();
        removeTest();
        new ListPerformanceTest(JdkListsPerformanceTest.DEFAULT_TEST_SIZE, "Lesson4_DDL_PerformanceTest",
                new DoublyLinkedList<String>(), new LinkedList<String>()).doPerformanceTest();
    }

    private static void sizeTest() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1);
        assertEquals("DoublyLinkedListTest.size", list.size(), 1);
    }

    private static void getTest() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1);
        assertEquals("DoublyLinkedListTest.get", 1, list.get(0));

        try {
            list.get(100);
            fail();
        } catch (Throwable e) {
            assertEquals("DoublyLinkedListTest.get", true, e instanceof ArrayIndexOutOfBoundsException);
        }
    }

    private static void addTest() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1);
        assertEquals("DoublyLinkedListTest.add", 1, list.get(list.size()));
        list.add(0, 10);
        assertEquals("DoublyLinkedListTest.add", 10, list.get(0));
    }

    private static void containsTest() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1);
        assertEquals("DoublyLinkedListTest.contains", true, list.contains(1));
        assertEquals("DoublyLinkedListTest.contains", false, list.contains(1000000));
    }

    private static void indexOfTest() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1);
        assertEquals("DoublyLinkedListTest.indexOf", 0, list.indexOf(1));
        assertEquals("DoublyLinkedListTest.indexOf", -1, list.indexOf(1000000));
    }

    private static void removeTest() {
        DoublyLinkedList<String> list = new DoublyLinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("six");
        list.add("seven");

        assertEquals("DoublyLinkedListTest.remove", true, list.remove("one"));
        assertEquals("DoublyLinkedListTest.remove", false, list.remove("one"));
        assertEquals("DoublyLinkedListTest.remove", "seven", list.remove(5));

        try {
            list.remove(100);
            fail();
        } catch (Throwable e) {
            assertEquals("DoublyLinkedListTest.remove", true, e instanceof IndexOutOfBoundsException);
        }
    }
}
