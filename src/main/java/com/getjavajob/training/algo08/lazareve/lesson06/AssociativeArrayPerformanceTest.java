package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import main.java.com.getjavajob.training.algo08.lazareve.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Helper.writeToFile;

public class AssociativeArrayPerformanceTest {

    private static final int TEST_SIZE = 4000001;

    public static void main(String[] args) {
        performanceTest();
    }

    private static void performanceTest() {
        AssociativeArray<String, Integer> assocArr = new AssociativeArray<>();
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < TEST_SIZE; i++) {
            map.put("" + i, i);
            assocArr.add("" + i, i);
        }

        StopWatch watcher = new StopWatch();
        StringBuilder sb = new StringBuilder();
        sb.append(addTest(watcher, assocArr, map));
        sb.append(getTest(watcher, assocArr, map));
        sb.append(removeTest(watcher, assocArr, map));
        writeToFile("AssociativeArrayPerformanceTest", sb.toString());
    }

    private static String addTest(StopWatch sw, AssociativeArray aa, Map map) {
        StringBuilder sb = new StringBuilder("\nAdd test\n");

        sw.start();
        aa.add("polygenelubricants", 999);
        aa.add("GydZG_", 999);
        aa.add("DESIGNING WORKHOUSES", 999);
        long assocTime = sw.getElapsedTime();

        sw.start();
        map.put("polygenelubricants", 999);
        map.put("GydZG_", 999);
        map.put("DESIGNING WORKHOUSES", 999);
        long mapTime = sw.getElapsedTime();

        sb.append("AssociativeArray: " + assocTime + " ms\n");
        sb.append("HashMap: " + mapTime + " ms");

        return sb.toString();
    }

    private static String getTest(StopWatch sw, AssociativeArray aa, Map map) {
        StringBuilder sb = new StringBuilder("\nGet test\n");

        sw.start();
        aa.get("polygenelubricants");
        long assocTime = sw.getElapsedTime();

        sw.start();
        map.get("polygenelubricants");
        long mapTime = sw.getElapsedTime();

        sb.append("AssociativeArray: " + assocTime + " ms\n");
        sb.append("HashMap: " + mapTime + " ms");

        return sb.toString();
    }

    private static String removeTest(StopWatch sw, AssociativeArray aa, Map map) {
        StringBuilder sb = new StringBuilder("\nGet test\n");

        sw.start();
        aa.remove("polygenelubricants");
        long assocTime = sw.getElapsedTime();

        sw.start();
        map.remove("polygenelubricants");
        long mapTime = sw.getElapsedTime();

        sb.append("AssociativeArray: " + assocTime + " ms\n");
        sb.append("HashMap: " + mapTime + " ms");

        return sb.toString();
    }
}
