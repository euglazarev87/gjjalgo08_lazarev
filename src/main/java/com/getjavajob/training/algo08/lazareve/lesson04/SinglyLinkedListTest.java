package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import java.util.ArrayList;
import java.util.List;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
        addGetTest();
        asListTest();
        sizeTest();
        reverseTest();
    }

    private static void reverseTest() {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.reverse();

        assertEquals("SinglyLinkedList.reverse()", "7", list.get(0));
        assertEquals("SinglyLinkedList.reverse()", "6", list.get(1));
        assertEquals("SinglyLinkedList.reverse()", "1", list.get(6));
    }

    private static void sizeTest() {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        assertEquals("SinglyLinkedList.size()", 3, list.size());
    }

    private static void asListTest() {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");

        SinglyLinkedList<String> llist = new SinglyLinkedList<>();
        for (String el: list) {
            llist.add(el);
        }

        assertEquals("SinglyLinkedList.asList()", true, llist.asList().containsAll(list));
        assertEquals("SinglyLinkedList.asList()", false, llist.asList().contains("9"));
    }

    private static void addGetTest() {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");

        assertEquals("SinglyLinkedList.add()", "a", list.get(0));
        assertEquals("SinglyLinkedList.add()", "b", list.get(1));
        assertEquals("SinglyLinkedList.add()", "d", list.get(3));
        try {
            assertEquals("SinglyLinkedList.add()", "d", list.get(4));
            fail();
        } catch (Exception e) {
            assertEquals("SinglyLinkedList.add()", true, e instanceof IndexOutOfBoundsException);
        }
    }
}
