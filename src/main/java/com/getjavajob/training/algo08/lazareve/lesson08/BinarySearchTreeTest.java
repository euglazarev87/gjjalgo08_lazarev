package main.java.com.getjavajob.training.algo08.lazareve.lesson08;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.search.BinarySearchTree;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class BinarySearchTreeTest {

    public static void main(String[] args) {
        findNodeTest();
        treeSearchTest();
        addRootTest();
        addTest();
        addLeftRightTest();
        removeTest();
    }

    private static void findNodeTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(5);
        Node<Integer> n15 = tree.add(15);
        tree.add(4);
        tree.add(7);
        tree.add(16);
        tree.add(14);
        assertEquals("BinarySearchTree.findNode()", n15, tree.findNode(15));
    }

    private static void treeSearchTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(5);
        Node<Integer> n15 = tree.add(15);
        tree.add(4);
        tree.add(7);
        tree.add(16);
        tree.add(14);

        assertEquals("BinarySearchTree.treeSearch()", 14, tree.treeSearch(n15, 14).getElement());
        assertEquals("BinarySearchTree.treeSearch()", true, tree.treeSearch(n15, 20) == null);
    }

    private static void addRootTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        try {
            tree.addRoot(100);
            fail();
        } catch (Throwable e) {
            assertEquals("BinarySearchTree.addRoot()", true, e instanceof UnsupportedOperationException);
        }
    }

    private static void addTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n = tree.add(100);
        assertEquals("BinarySearchTree.add()", true, tree.findNode(100) != null);
        try {
            tree.add(n, 150);
            fail();
        } catch (Throwable e) {
            assertEquals("BinarySearchTree.add()", true, e instanceof UnsupportedOperationException);
        }
    }

    private static void addLeftRightTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n = tree.add(100);

        try {
            tree.addLeft(n, 150);
            fail();
        } catch (Throwable e) {
            assertEquals("BinarySearchTree.addLeft()", true, e instanceof UnsupportedOperationException);
        }

        try {
            tree.addRight(n, 150);
            fail();
        } catch (Throwable e) {
            assertEquals("BinarySearchTree.addRight()", true, e instanceof UnsupportedOperationException);
        }
    }

    private static void removeTest() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(5);
        Node<Integer> n15 = tree.add(15);
        tree.add(4);
        tree.add(7);
        tree.add(16);
        tree.add(14);

        try {
            tree.remove(n15);
            fail();
        } catch (Throwable e) {
            assertEquals("BinarySearchTree.remove()", true, e instanceof UnsupportedOperationException);
        }

        assertEquals("BinarySearchTree.remove()", true, tree.findNode(10) != null);
        tree.remove(10);
        assertEquals("BinarySearchTree.remove()", false, tree.findNode(10) != null);
    }
}
