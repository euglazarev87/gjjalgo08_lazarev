package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import main.java.com.getjavajob.training.algo08.lazareve.util.Assert;

public class LinkedListStackTest {

    public static void main(String[] args) {

        Stack<String> stack = new LinkedListStack<>();
        stack.push("a");
        stack.push("b");
        stack.push("c");
        stack.push("d");

        Assert.assertEquals("LinkedListStack.add() and pop()", "d", stack.pop());
        Assert.assertEquals("LinkedListStack.add() and pop()", "c", stack.pop());
    }
}
