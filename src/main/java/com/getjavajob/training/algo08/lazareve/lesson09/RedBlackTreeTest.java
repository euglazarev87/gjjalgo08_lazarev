package main.java.com.getjavajob.training.algo08.lazareve.lesson09;

import main.java.com.getjavajob.training.algo.tree.binary.search.balanced.RedBlackTree;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class RedBlackTreeTest {

    public static void main(String[] args) {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.add(100);
        tree.add(50);
        tree.add(40);
        tree.add(150);
        tree.add(160);
        tree.add(90);

        assertEquals("RedBlackTree.add()", 6, tree.size());
        assertEquals("RedBlackTree.add()", tree.findNode(50), tree.root());

        tree.remove(90);
        tree.remove(50);
        assertEquals("RedBlackTree.remove()", 4, tree.size());
        assertEquals("RedBlackTree.remove()", tree.findNode(100), tree.root());
    }
}
