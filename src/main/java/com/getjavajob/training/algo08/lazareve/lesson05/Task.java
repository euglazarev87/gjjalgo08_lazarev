package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public interface Task<T> {
    void doTask(T el);
}
