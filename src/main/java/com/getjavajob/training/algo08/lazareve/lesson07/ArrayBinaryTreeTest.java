package main.java.com.getjavajob.training.algo08.lazareve.lesson07;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.AbstractBinaryTree;
import main.java.com.getjavajob.training.algo.tree.binary.ArrayBinaryTree;
import main.java.com.getjavajob.training.algo.tree.binary.BinaryTree;

import java.util.Iterator;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class ArrayBinaryTreeTest {

    public static void main(String[] args) {
        addRootTest();
        addTest();
        addLeftTest();
        addRightTest();
        parentTest();
        setTest();
        removeTest();
        leftTest();
        rightTest();
        rootTest();
        sizeTest();
        iteratorTest();
        nodesTest();
        siblingTest();
        childrenTest();
        childrenNumberTest();
        isInternalTest();
        isExternalTest();
        isRootTest();
        isEmptyTest();
        orderTest();
    }

    private static void addRootTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        assertEquals("LinkedBinaryTree.addRoot()", "root", tree.addRoot("root").getElement());

        try {
            tree.addRoot("anotherRoot");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addRoot()", "Root is not empty", e.getMessage());
        }
    }

    private static void addTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        assertEquals("LinkedBinaryTree.add()", "fuflo", tree.add(tree.addRoot("fuf"), "fuflo").getElement());
    }

    private static void addLeftTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        tree.addLeft(tree.root(), "left");
        assertEquals("LinkedBinaryTree.addLeft()", "left", tree.left(tree.root()).getElement());

        try {
            tree.addLeft(tree.root(), "l");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addLeft()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void addRightTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        tree.addRight(tree.root(), "right");
        assertEquals("LinkedBinaryTree.addRight()", "right", tree.right(tree.root()).getElement());

        try {
            tree.addRight(tree.root(), "r");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addRight()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void parentTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> n = tree.add(tree.root(), "right");
        assertEquals("LinkedBinaryTree.parent()", tree.root(), tree.parent(n));
    }

    private static void setTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> n = tree.add(tree.root(), "right");
        assertEquals("LinkedBinaryTree.set()", "right", tree.set(n, "left"));
    }

    private static void removeTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");

        try {
            tree.remove(tree.root());
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.remove()", true, e instanceof UnsupportedOperationException);
        }
    }

    private static void leftTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "leftCH");
        assertEquals("LinkedBinaryTree.left()", "leftCH", left.getElement());
    }

    private static void rightTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> right = tree.addRight(tree.root(), "rightCh");
        assertEquals("LinkedBinaryTree.right()", "rightCh", right.getElement());
    }

    private static void rootTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.root()", "root", tree.root().getElement());
    }

    private static void sizeTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.size()", 1, tree.size());
    }

    private static void iteratorTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");

        Iterator<String> it = tree.iterator();
        assertEquals("LinkedBinaryTree.iterator()", true, it.hasNext());
        it.next();
        assertEquals("LinkedBinaryTree.iterator()", false, it.hasNext());
    }

    private static void nodesTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        tree.addLeft(tree.root(), "left");
        tree.addRight(tree.root(), "right");

        StringBuilder sb = new StringBuilder();
        for (Node<String> node : tree.nodes()) {
            sb.append(node.getElement());
        }
        assertEquals("LinkedBinaryTree.nodes()", "rootleftright", sb.toString());
    }

    private static void siblingTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "left");
        Node<String> right = tree.addRight(tree.root(), "right");

        assertEquals("LinkedBinaryTree.sibling()", left, tree.sibling(right));
    }

    private static void childrenTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");

        StringBuilder sb = new StringBuilder();
        for (Node<String> node : tree.children(tree.root())) {
            sb.append(node.getElement());
        }

        assertEquals("LinkedBinaryTree.children()", "12", sb.toString());
    }

    private static void childrenNumberTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.childrenNumber()", 0, tree.childrenNumber(tree.root()));
    }

    private static void isInternalTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");
        tree.addLeft(right, "22");
        assertEquals("LinkedBinaryTree.isInternal()", true, tree.isInternal(right));
        assertEquals("LinkedBinaryTree.isInternal()", false, tree.isInternal(left));
    }

    private static void isExternalTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");
        tree.addLeft(right, "22");
        assertEquals("LinkedBinaryTree.isExternal()", false, tree.isExternal(right));
        assertEquals("LinkedBinaryTree.isExternal()", true, tree.isExternal(left));
    }

    private static void isRootTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> r = tree.addRoot("root");
        assertEquals("LinkedBinaryTree.isRoot()", true, tree.isRoot(r));
    }

    private static void isEmptyTest() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> r = tree.addRoot("root");
        assertEquals("LinkedBinaryTree.isEmpty()", false, tree.isEmpty());
    }

    private static void orderTest() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        Node<String> b = tree.addLeft(tree.root(), "B");
        Node<String> g = tree.addRight(tree.root(), "G");

        Node<String> a = tree.addLeft(b, "A");
        Node<String> d = tree.addRight(b, "D");
        tree.addLeft(d, "C");
        tree.addRight(d, "E");

        Node<String> i = tree.addRight(g, "I");
        tree.addLeft(i, "H");

        StringBuilder sb = new StringBuilder();

        // pre order
        for (Node<String> s : tree.preOrder()) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.preOrder()", "FBADCEGIH", sb.toString());
        sb.replace(0, sb.length(), "");

        // in order
        for (Node<String> s : tree.inOrder()) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.inOrder()", "ABCDEFGHI", sb.toString());
        sb.replace(0, sb.length(), "");

        // post order
        for (Node<String> s : tree.postOrder()) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.postOrder()", "ACEDBHIGF", sb.toString());
        sb.replace(0, sb.length(), "");

        // breadthFirst order
        for (Node<String> s : tree.breadthFirst()) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.breadthFirst()", "FBGADICEH", sb.toString());
    }
}
