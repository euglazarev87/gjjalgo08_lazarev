package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public interface Transformator<I, O> {
    O transform(I source);
}
