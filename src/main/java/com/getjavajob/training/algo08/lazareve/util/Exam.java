package main.java.com.getjavajob.training.algo08.lazareve.util;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class Exam {

    public static void main(String[] args) {

        //Long i = 0x1_0000_0000l; //// TODO: 08.10.16 Посчитать минимальное количество бит для хранения
        //System.out.println(Long.toBinaryString(i).length());


//        Integer i = 20; // TODO: Разобраться что такое кеш чисел
//        Integer j = Integer.valueOf(20);
//        Integer k = new Integer(20);

        // TODO: Неизменяемость строк (кеш хеш)

        // Object[] arr = new String[] {""};
        // arr[0] = 1;

        // List<Object> gen = new ArrayList<String>();
        //

        /*static void m(List<Object> l) {
            l.add("string");
        }*/

        /*static void m(List<?> l) {
            l.add(new Object());
        }*/

        /*List<String> boysAndGirls = Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша");
        for (String boyOrGirl : boysAndGirls) {
            if ("Саша".equals(boyOrGirl)) {
                boysAndGirls.add("Маша");
            }
        }
        System.out.println(boysAndGirls.size()); // ?*/ // TODO: 2 грабельки


        /*
        ThirdPartyType a = ...;
        ThirdPartyType b = ...;
        ThirdPartyType c = ...;
        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 3

        hashSet.add(b);
        hashSet.add(a);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 2 - when is it possible?*/ // TODO: Разобраться

        // TODO: HashMap put()

         /* TreeSet<Object> treeSet = ...;
         treeSet.add(new Object());
         System.out.println(treeSet.size()); // size ? */ // TODO: TreeSet put()

        // boolean containsDuplicates(int[] numbers) {}
        // 3 diff solutions with 2 different speeds,
        // 1 of them must not use O(n) memory, all of them faster n^2

        // TODO: UnitTests on 10 lesson
        // TODO: допилить код

        StopWatch one = new StopWatch();
        StopWatch two = new StopWatch();

        Map<StopWatch, Integer> map = new TreeMap<>();
        map.put(one, 1);
        map.put(two, 2);


    }

}