package main.java.com.getjavajob.training.algo08.lazareve.lesson08;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.search.balanced.BalanceableTree;
import main.java.com.getjavajob.training.algo08.lazareve.util.Assert;

public class BalanceableTreeTest {

    public static void main(String[] args) {
        rotateTwice();
    }

    private static void rotateTwice() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        tree.add(3);
        tree.add(7);
        Node<Integer> pivot = tree.add(5);
        tree.reduceSubtreeTest(pivot);

        Assert.assertEquals("BalanceableTree.rotate()", 5, tree.root().getElement());
    }
}
