package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class ExpressionCalculatorTest {

    public static void main(String[] args) {
        expressionCalculatorTest();
    }

    private static void expressionCalculatorTest() {
        ExpressionCalculator a = new ExpressionCalculator();
        assertEquals("ExpressionCalculator.doTransform()", 3.0, a.evaluateExpression("1 + 1 * 2"));
        assertEquals("ExpressionCalculator.doTransform()", 5.0, a.evaluateExpression("1 + ((1 * 2) * 2)"));
        assertEquals("ExpressionCalculator.doTransform()", 3.0, a.evaluateExpression("1 + ((1 * 2) * 2) / 2"));
    }
}
