package main.java.com.getjavajob.training.algo08.lazareve.lesson01;

import static main.java.com.getjavajob.training.algo08.lazareve.lesson01.Task6.*;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class Task6Test {

    public static void main(String[] args) {
        xorToTwoTest();
        getSumOfXorOfTwo();
        resetLowerBitsTest();
        setBitToOnTest();
        invertBitTest();
        setBitToZero();
        getLowerBitsTest();
    }

    private static void getLowerBitsTest() {
        assertEquals("Task6.getLowerBits()", 0b10, getLowerBits(0b1010, 2));
    }

    private static void setBitToZero() {
        assertEquals("Task6.setGivenBitWithZero()", 0b1000, setGivenBitWithZero(0b1010, 2));
    }

    private static void invertBitTest() {
        assertEquals("Task6.invertBit()", 0b1011, invertBit(0b1010, 1));
    }

    private static void setBitToOnTest() {
        assertEquals("Task6.setGivenBitWithOne()", 0b1011, setGivenBitWithOne(0b1010, 1));
    }

    private static void resetLowerBitsTest() {
        assertEquals("Task6.resetLowerBits()", 0b01100000, resetLowerBits(0b01101111, 4));
    }

    private static void getSumOfXorOfTwo() {
        assertEquals("Task6.getXorToM()", 0b01011, getXorToM(1, 0b1010));
    }

    private static void xorToTwoTest() {
        assertEquals("Task6.getXorToTwo()", 0b011, getXorToTwo(1));
    }
}
