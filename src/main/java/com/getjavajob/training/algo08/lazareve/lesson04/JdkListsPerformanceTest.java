package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import main.java.com.getjavajob.training.algo08.lazareve.util.ListPerformanceTest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdkListsPerformanceTest extends ListPerformanceTest {

    public static final int DEFAULT_TEST_SIZE = 1000000;

    public JdkListsPerformanceTest(int sizeOfData, String fileName, List<String> subject1, List<String> subject2) {
        super(sizeOfData, fileName, subject1, subject2);
    }

    public static void main(String[] args) {
        new JdkListsPerformanceTest(DEFAULT_TEST_SIZE, "JdkListsPerformanceTest", new ArrayList<String>(), new LinkedList<String>()).doPerformanceTest();
    }
}
