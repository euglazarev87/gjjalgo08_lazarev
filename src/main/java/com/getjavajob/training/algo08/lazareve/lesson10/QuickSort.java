package main.java.com.getjavajob.training.algo08.lazareve.lesson10;

import java.util.Arrays;

import static main.java.com.getjavajob.training.algo08.lazareve.util.MathHelper.safeAdd;
import static main.java.com.getjavajob.training.algo08.lazareve.util.MathHelper.safeDivide;

public class QuickSort {

    public static void main(String[] args) {
        Integer[] l = new Integer[]{4, 9, 7, 6, 8, 1, 3, 2, 5, 4};
        sort(l);
        System.out.println(Arrays.toString(l));
    }

    public static <T extends Comparable<T>> void sort(T[] list) {
        sort(list, 0, list.length - 1);
    }

    private static <T extends Comparable<T>> void sort(T[] list, int l, int h) {
        if (l < h) {
            int pivot = partition(list, l, h);
            sort(list, l, pivot);
            sort(list, pivot + 1, h);
        }
    }

    private static <T extends Comparable<T>> int partition(T[] list, int l, int h) {
        T pivotVal = list[(l + h) >>> 1];
        //T pivotVal = list[safeDivide(safeAdd(l, h), 2)];
        while (true) {
            while (list[l].compareTo(pivotVal) < 0) {
                l++;
            }
            while (list[h].compareTo(pivotVal) > 0) {
                h--;
            }
            if (l >= h) {
                return h;
            }
            swap(list, l, h);
        }
    }

    private static <T> void swap(T[] list, int i, int j) {
        T temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }
}
