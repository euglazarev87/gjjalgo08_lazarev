package main.java.com.getjavajob.training.algo08.lazareve.lesson09;


import java.util.Arrays;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.Set;

public class CityFinder {

    private static NavigableSet<String> tree;

    static {
        tree = new TreeSet<>(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                String lower1 = (String) o1;
                String lower2 = (String) o2;
                return lower1.compareToIgnoreCase(lower2);
            }
        });
        tree.add("Moskow");
        tree.add("Bishkek");
        tree.add("Mogilev");
        tree.add("Barcelona");
        tree.add("Milan");
        tree.add("Paris");
        tree.add("Perm");
        tree.add("Parma");
        tree.add("Novosibirsk");
        tree.add("Narva");
        tree.add("Nalchik");
    }

    public static void main(String[] args) {
        String substr = "mo";
        System.out.println(Arrays.toString(findCity(substr).toArray()));
    }

    private static Set<String> findCity(String substr) {
        substr = substr.toLowerCase();
        String top = tree.ceiling(substr);
        char lastChar = substr.charAt(substr.length()-1);
        char nextChar = (char) (lastChar + 1);
        String floor = tree.floor(substr.replace(lastChar, nextChar));

        return tree.subSet(top, true, floor, true);
    }
}
