package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public class LinkedListQueue<T> extends LinkedListQueueAbst<T> {

    private SinglyLinkedList<T> list;

    public LinkedListQueue() {
        list = new SinglyLinkedList<>();
    }

    @Override
    public boolean add(T t) {
        list.head = new SinglyLinkedList.Node<>(t, list.head);
        return true;
    }

    @Override
    public T remove() {
        return list.removeLast();
    }
}
