package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

/**
 * Class for testing Deque methods with ArrayDeque implementation
 * Tests only methods not tested in Queue test
 */
public class DequeTest {

    private static final Deque<Integer> INTEGER_LIST;
    private static final Deque<String> STRING_LIST;

    static {
        INTEGER_LIST = new ArrayDeque<>();
        INTEGER_LIST.add(1);
        INTEGER_LIST.add(2);
        INTEGER_LIST.add(3);
        INTEGER_LIST.add(4);
        INTEGER_LIST.add(5);
        INTEGER_LIST.add(6);
        INTEGER_LIST.add(7);
        INTEGER_LIST.add(8);
        INTEGER_LIST.add(9);
        INTEGER_LIST.add(10);

        STRING_LIST = new ArrayDeque<>();
        STRING_LIST.add("1");
        STRING_LIST.add("2");
        STRING_LIST.add("3");
        STRING_LIST.add("4");
        STRING_LIST.add("5");
        STRING_LIST.add("6");
        STRING_LIST.add("7");
        STRING_LIST.add("8");
        STRING_LIST.add("9");
        STRING_LIST.add("10");
    }

    public static void main(String[] args) {
        addFirstTest();
        addLastTest();
        offerFirstTest();
        offerLastTest();
        removeFirstTest();
        removeLastTest();
        pollFirstTest();
        pollLastTest();
        getFirstTest();
        getLastTest();
        peekFirstTest();
        peekLastTest();
        removeFirstOccurrenceTest();
        removeLastOccurrenceTest();
        pushTest();
        popTest();
        sizeTest();
    }

    private static void addFirstTest() {
        Deque<String> list = new ArrayDeque<>();
        list.add("HTML");
        list.add("CSS");
        list.add("JavaScript");
        list.addFirst("Java");
        assertEquals("ArrayDeque.addFirst()", "Java", list.getFirst());
    }

    private static void addLastTest() {
        Deque<String> list = new ArrayDeque<>();
        list.add("HTML");
        list.add("CSS");
        list.add("JavaScript");
        list.addFirst("Java");
        assertEquals("ArrayDeque.addLast()", "JavaScript", list.getLast());
    }

    private static void offerFirstTest() {
        Deque<String> list = new ArrayDeque<>();
        list.add("HTML");
        list.add("CSS");
        list.add("JavaScript");
        list.offerFirst("Java");
        assertEquals("ArrayDeque.offerFirst()", "Java", list.getFirst());
    }

    private static void offerLastTest() {
        Deque<String> list = new ArrayDeque<>();
        list.add("HTML");
        list.add("CSS");
        list.offerLast("JavaScript");
        assertEquals("ArrayDeque.offerLast()", "JavaScript", list.getLast());
    }

    private static void removeFirstTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.removeFirst();
        assertEquals("ArrayDeque.removeFirst()", "2", list.getFirst());
    }

    private static void removeLastTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.removeLast();
        assertEquals("ArrayDeque.removeLast()", "9", list.getLast());
    }

    private static void pollFirstTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.pollFirst()", "1", list.pollFirst());
        assertEquals("ArrayDeque.pollFirst()", "2", list.getFirst());
    }

    private static void pollLastTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.pollLast()", "10", list.pollLast());
        assertEquals("ArrayDeque.pollLast()", "9", list.getLast());
    }

    private static void getFirstTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.getFirst()", "1", list.getFirst());
    }

    private static void getLastTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.getLast()", "10", list.getLast());
    }

    private static void peekFirstTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.peekFirst()", "1", list.peekFirst());
        assertEquals("ArrayDeque.peekFirst()", "1", list.getFirst());
    }

    private static void peekLastTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.peekLast()", "10", list.peekLast());
        assertEquals("ArrayDeque.peekLast()", "10", list.getLast());
    }

    private static void removeFirstOccurrenceTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.addLast("1");
        assertEquals("ArrayDeque.removeFirstOccurrence()", true, list.removeFirstOccurrence("1"));
        assertEquals("ArrayDeque.removeFirstOccurrence()", "2", list.getFirst());
        assertEquals("ArrayDeque.removeFirstOccurrence()", "1", list.getLast());
    }

    private static void removeLastOccurrenceTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.addLast("1");
        assertEquals("ArrayDeque.removeLastOccurrence()", true, list.removeLastOccurrence("1"));
        assertEquals("ArrayDeque.removeLastOccurrence()", "1", list.getFirst());
        assertEquals("ArrayDeque.removeLastOccurrence()", "10", list.getLast());
    }

    private static void pushTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.push("java");
        assertEquals("ArrayDeque.push()", "java", list.getFirst());
    }

    private static void popTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        list.push("java");
        assertEquals("ArrayDeque.pop()", "java", list.pop());
        assertEquals("ArrayDeque.pop()", "1", list.getFirst());
    }

    private static void sizeTest() {
        Deque<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.size()", 10, list.size());
    }
}
