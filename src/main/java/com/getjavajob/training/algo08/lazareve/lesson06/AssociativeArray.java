package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

public class AssociativeArray<K, V> implements IAssocArray<K, V> {

    private static final float LOAD_FACTOR_BY_DEFAULT = 0.7f;
    private static final int ARRAY_SIZE_BY_DEFAULT = 16;
    private static final int ARRAY_GROWING_VALUE = 2;

    private Entry<K, V>[] arr;
    private int size;

    public AssociativeArray() {
        arr = new Entry[ARRAY_SIZE_BY_DEFAULT];
        size = 0;
    }

    @Override
    public V add(K key, V value) {
        V oldValue = null;
        int index = key == null ? 0 : getIndexForKey(key);
        if (arr[index] == null) {
            arr[index] = new Entry<>(key, value);
            size++;
        } else {
            Entry<K, V> v = arr[index];
            while (v != null) {
                if ((key == null && v.key == null) || key.equals(v.key)) {
                    oldValue = v.value;
                    v.value = value;
                    break;
                }
                v = v.next;
            }
            if (oldValue == null) {
                arr[index] = new Entry(key, value, arr[index]);
                size++;
            }
        }

        checkCapacity();
        return oldValue;
    }

    @Override
    public V get(K key) {
        int index = key == null ? 0 : getIndexForKey(key);
        Entry<K, V> v = arr[index];
        while (v != null) {
            if ((key == null && v.key == null) || key.equals(v.key)) {
                return v.value;
            }
            v = v.next;
        }
        return null;
    }

    @Override
    public V remove(K key) {
        V value = null;
        int index = key == null ? 0 : getIndexForKey(key);
        Entry<K, V> v = arr[index];
        Entry<K, V> prev = arr[index];
        while (v != null) {
            if ((key == null && v.key == null) || key.equals(v.key)) {
                value = v.value;
                prev.next = v.next;
                size--;
                if (arr[index] == v) {
                    arr[index] = v.next;
                }
                break;
            }
            prev = v;
            v = v.next;
        }
        return value;
    }

    private int getIndexForKey(K key) {
        //return key.hashCode() % arr.length; // commented because takes negative indices
        return key.hashCode() & (arr.length - 1);
    }

    private void checkCapacity() {
        float loadFactor = (float) size / arr.length;
        if (loadFactor >= LOAD_FACTOR_BY_DEFAULT) {
            resize();
        }
    }

    private void resize() {
        Entry[] oldTable = arr;
        arr = new Entry[arr.length * ARRAY_GROWING_VALUE];
        size = 0;
        for (int i = 0; i < oldTable.length; i++) {
            if (oldTable[i] == null) {
                continue;
            }
            Entry<K, V> v = oldTable[i];
            while (v != null) {
                add(v.key, v.value);
                v = v.next;
            }
        }
    }

    public Entry<K, V>[] getEntrySet() {
        Entry<K, V>[] list = new Entry[size];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null) {
                continue;
            }
            Entry<K, V> v = arr[i];
            while (v != null) {
                list[j] = v;
                j++;
                v = v.next;
            }
        }
        return list;
    }

    static class Entry<K, V> {

        K key;
        V value;
        Entry<K, V> next;

        public Entry(K k, V v, Entry<K, V> n) {
            key = k;
            value = v;
            next = n;
        }

        public Entry(K k, V v) {
            this(k, v, null);
        }

        @Override
        public String toString() {
            return "Key: " + key + " Value: " + value;
        }
    }
}
