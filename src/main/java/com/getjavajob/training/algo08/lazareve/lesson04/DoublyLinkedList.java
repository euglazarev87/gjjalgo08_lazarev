package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<V> extends DoublyLinkedListAbst<V> {

    private Element<V> first;
    private Element<V> last;
    private int size;

    public DoublyLinkedList() {
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Iterator<V> iterator() {
        return new ListIteratorImpl();
    }

    @Override
    public boolean add(V v) {
        Element<V> lastElement = last;
        Element<V> newElement = new Element<>(v, last, null);
        last = newElement;
        if (lastElement == null) {
            first = newElement;
        } else {
            lastElement.next = newElement;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (indexOf(o) == -1) {
            return false;
        }
        remove(indexOf(o));
        return true;
    }

    @Override
    public V get(int index) {
        checkSize(index);
        return findElementByIndex(index).value;
    }

    @Override
    public void add(int index, V value) {
        checkSize(index);
        if (index == size) {
            add(value);
        } else {
            Element<V> predElement = findElementByIndex(index);
            Element<V> predPrev = predElement.prev;
            Element<V> newElement = new Element<>(value, predPrev, predElement);
            predElement.prev = newElement;
            if (predPrev == null) {
                first = newElement;
            } else {
                predPrev.next = newElement;
            }
            size++;
        }
    }

    private Element<V> findElementByIndex(int index) {
        if (index < (size / 2)) {
            Element<V> e = first;
            for (int i = 0; i < index; i++) {
                e = e.next;
            }
            return e;
        } else {
            Element<V> e = last;
            for (int i = size - 1; i > index; i--) {
                e = e.prev;
            }
            return e;
        }
    }

    private void checkSize(int i) {
        if (i > size || i < 0) {
            throw new ArrayIndexOutOfBoundsException("Index: " + i + " Size: " + size());
        }
    }

    @Override
    public V remove(int index) {
        checkSize(index);
        Element<V> removedEl = findElementByIndex(index);
        Element<V> prev = removedEl.prev;
        Element<V> next = removedEl.next;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
        }
        size--;
        return removedEl.value;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for (Element<V> el = first; el != null; el = el.next) {
            if (o.equals(el.value)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Element<V> e = first;
        while (e != null) {
            sb.append(e.value + ",");
            e = e.next;
        }
        return sb.toString();
    }

    private static class Element<V> {
        Element<V> prev;
        Element<V> next;
        V value;

        public Element(V value, Element<V> prev, Element<V> next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
    }

    private class ListIteratorImpl implements ListIterator<V> {

        int nextIndex;
        Element<V> nextEl;
        Element<V> lastReturned;

        public ListIteratorImpl() {
            nextEl = first;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public V next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            lastReturned = nextEl;
            nextEl = nextEl.next;
            nextIndex++;
            return lastReturned.value;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public V previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastReturned = nextEl = (nextEl == null) ? last : nextEl.prev;
            nextIndex--;
            return lastReturned.value;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }

            Element<V> returnedNext = lastReturned.next;
            DoublyLinkedList.this.remove(lastReturned);
            if (lastReturned == nextEl) {
                nextEl = returnedNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
        }

        @Override
        public void set(V v) {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            lastReturned.value = v;
        }

        @Override
        public void add(V v) {
            DoublyLinkedList.this.add(nextIndex, v);
            nextIndex++;
            lastReturned = null;
        }
    }
}
