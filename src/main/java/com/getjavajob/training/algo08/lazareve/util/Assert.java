package main.java.com.getjavajob.training.algo08.lazareve.util;

import main.java.com.getjavajob.training.algo.tree.Node;

import java.util.*;

public class Assert {

    public static void assertEquals(String methodName, boolean expected, boolean actual) {

        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, int expected, int actual) {

        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, double expected, double actual) {

        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, String expected, String actual) {

        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, Integer[] expected, Integer[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static <T> void assertEquals(String methodName, T[] expected, T[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static <T> void assertEquals(String methodName, List<T> expected, List<T> actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }

    public static <T> void assertEquals(String methodName, Queue<T> expected, Queue<T> actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }

    public static <K, V> void assertEquals(String methodName, SortedMap<K, V> expected, SortedMap<K, V> actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }

    public static <K, V> void assertEquals(String methodName, Map.Entry<K, V> expected, Map.Entry<K, V> actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }

    public static <T> void assertEquals(String methodName, Node<T> expected, Node<T> actual) {
        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }

    public static void nullAssertEquals(String methodName, Object actual) {
        if (actual == null) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected null " + actual.toString());
        }
    }

    public static void fail() {
        throw new AssertionError();
    }
}
