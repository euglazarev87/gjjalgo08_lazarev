package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class ExpressionCalculator {

    private static final char[] OPERATORS = new char[]{'+', '-', '/', '*', '(', ')'};;

    private Queue<Character> output;
    private Stack<Character> stack;

    public ExpressionCalculator() {
        output = new ArrayDeque<>();
        stack = new Stack<>();
    }

    public double evaluateExpression(String exp) {
        doTransform(exp);
        if (output.isEmpty()) {
            throw new IllegalStateException("Expression is empty");
        }

        Stack<Double> resultStack = new Stack<>();
        while (!output.isEmpty()) {
            char character = output.poll();

            if (!isOperator(character)) {
                resultStack.push(Double.parseDouble(String.valueOf(character)));
                continue;
            }
            double firstOperand = 0;
            double secondOperand = 0;
            switch (character) {
                case '+':
                    firstOperand = resultStack.pop();
                    secondOperand = resultStack.pop();
                    resultStack.push(secondOperand + firstOperand);
                    break;
                case '-':
                    firstOperand = resultStack.pop();
                    secondOperand = resultStack.pop();
                    resultStack.push(secondOperand - firstOperand);
                    break;
                case '*':
                    firstOperand = resultStack.pop();
                    secondOperand = resultStack.pop();
                    resultStack.push(secondOperand * firstOperand);
                    break;
                case '/':
                    firstOperand = resultStack.pop();
                    secondOperand = resultStack.pop();
                    resultStack.push(secondOperand / firstOperand);
                    break;
            }
        }
        return resultStack.pop();
    }

    private int getOperatorPrecedence(Character c) {
        switch (c) {
            case '*':
            case '/':
                return 3;
            case '+':
            case '-':
                return 2;
            case '(':
            case ')':
                return 1;
            default:
                return 0;
        }
    }

    private boolean isOperator(char c) {
        for (int i = 0; i < OPERATORS.length; i++) {
            if (OPERATORS[i] == c) {
                return true;
            }
        }
        return false;
    }

    private boolean doTransform(String exp) {

        exp = exp.replaceAll(" ", "");
        for (int i = 0; i < exp.length(); i++) {
            char token = exp.charAt(i);
            if (token == '(') {
                stack.push(token);
                continue;
            }

            if (Character.isDigit(token)) {
                output.add(token);
                continue;
            }

            if (isOperator(token)) {

                if (stack.isEmpty()) {
                    stack.push(token);
                    continue;
                }

                if (token == ')') {
                    while (stack.peek() != '(') {
                        output.add(stack.pop());
                    }
                    stack.pop(); // remove '('
                    continue;
                }

                if (getOperatorPrecedence(token) > getOperatorPrecedence(stack.peek())) {
                    stack.push(token);
                } else {
                    while (!stack.isEmpty() && getOperatorPrecedence(token) <= getOperatorPrecedence(stack.peek())) {
                        char last = stack.pop();
                        if (last != '(' && last != ')') {
                            output.add(last);
                        }
                    }
                }

            }
        }
        while (!stack.isEmpty()) {
            output.add(stack.pop());
        }
        return true;
    }

    private String getOutput() {
        StringBuilder sb = new StringBuilder();
        while (!output.isEmpty()) {
            sb.append(output.poll());
        }
        return sb.toString();
    }
}
