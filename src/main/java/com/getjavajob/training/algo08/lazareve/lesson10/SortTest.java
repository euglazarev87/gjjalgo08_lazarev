package main.java.com.getjavajob.training.algo08.lazareve.lesson10;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class SortTest {

    private static final int TEST_SIZE = 15000000;
    private static final Integer[] INTEGERS;
    private static final String[] STRINGS;

    static {
        INTEGERS = new Integer[TEST_SIZE];
        STRINGS = new String[TEST_SIZE];
        int j = 0;
        for (int i = TEST_SIZE; i > 0; i--, j++) {
            INTEGERS[j] = i;
            STRINGS[j] = "" + i;
        }
    }

    public static void main(String[] args) {
        quickSortTest();
        mergeSortTest();
        bubbleSortTest();
        insertionSortTest();
    }

    private static void insertionSortTest() {
        Integer[] list = {9, 0, 3, 8, 1, 7, 2, 6, 4, 5};
        InsertionSort.sort(list);
        assertEquals("InsertionSort.sort()", 0, list[0]);
        assertEquals("InsertionSort.sort()", 1, list[1]);
        assertEquals("InsertionSort.sort()", 2, list[2]);
    }

    private static void bubbleSortTest() {
        Integer[] list = {9, 0, 3, 8, 1, 7, 2, 6, 4, 5};
        BubbleSort.sort(list);
        assertEquals("Bubble.sort()", 0, list[0]);
        assertEquals("Bubble.sort()", 1, list[1]);
        assertEquals("Bubble.sort()", 2, list[2]);
    }

    private static void mergeSortTest() {
        Integer[] list = INTEGERS.clone();
        MergeSort.sort(list);
        assertEquals("MergeSort.sort()", 1, list[0]);
        assertEquals("MergeSort.sort()", 2, list[1]);
        assertEquals("MergeSort.sort()", 3, list[2]);
    }

    private static void quickSortTest() {
        Integer[] list = INTEGERS.clone();
        QuickSort.sort(list);
        assertEquals("QuickSort.sort()", 1, list[0]);
        assertEquals("QuickSort.sort()", 2, list[1]);
        assertEquals("QuickSort.sort()", 3, list[2]);
    }
}
