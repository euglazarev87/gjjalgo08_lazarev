package main.java.com.getjavajob.training.algo08.lazareve.lesson07;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.LinkedBinaryTree;

import java.util.Iterator;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class LinkedBinaryTreeTest {

    public static void main(String[] args) {
        addRootTest();
        addTest();
        addLeftTest();
        addRightTest();
        parentTest();
        setTest();
        removeTest();
        leftTest();
        rightTest();
        rootTest();
        sizeTest();
        iteratorTest();
        nodesTest();
        siblingTest();
        childrenTest();
        childrenNumberTest();
        isInternalTest();
        isExternalTest();
        isRootTest();
        isEmptyTest();
        orderTest();
    }

    private static void addRootTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        assertEquals("LinkedBinaryTree.addRoot()", "root", tree.addRoot("root").getElement());

        try {
            tree.addRoot("anotherRoot");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addRoot()", "Root is not empty", e.getMessage());
        }
    }

    private static void addTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        assertEquals("LinkedBinaryTree.add()", "fuflo", tree.add(tree.addRoot("fuf"), "fuflo").getElement());
    }

    private static void addLeftTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        tree.addLeft(tree.root(), "left");
        assertEquals("LinkedBinaryTree.addLeft()", "left", tree.left(tree.root()).getElement());

        try {
            tree.addLeft(tree.root(), "l");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addLeft()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void addRightTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        tree.addRight(tree.root(), "right");
        assertEquals("LinkedBinaryTree.addRight()", "right", tree.right(tree.root()).getElement());

        try {
            tree.addRight(tree.root(), "r");
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.addRight()", true, e instanceof IllegalArgumentException);
        }
    }

    private static void parentTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> n = tree.add(tree.root(), "right");
        assertEquals("LinkedBinaryTree.parent()", tree.root(), tree.parent(n));
    }

    private static void setTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> n = tree.add(tree.root(), "right");
        assertEquals("LinkedBinaryTree.set()", "right", tree.set(n, "left"));
    }

    private static void removeTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");

        try {
            tree.remove(tree.root());
            fail();
        } catch (Throwable e) {
            assertEquals("LinkedBinaryTree.remove()", true, e instanceof UnsupportedOperationException);
        }
    }

    private static void leftTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "leftCH");
        assertEquals("LinkedBinaryTree.left()", "leftCH", left.getElement());
    }

    private static void rightTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> right = tree.addRight(tree.root(), "rightCh");
        assertEquals("LinkedBinaryTree.right()", "rightCh", right.getElement());
    }

    private static void rootTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.root()", "root", tree.root().getElement());
    }

    private static void sizeTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.size()", 1, tree.size());
    }

    private static void iteratorTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");

        Iterator<String> it = tree.iterator();
        assertEquals("LinkedBinaryTree.iterator()", true, it.hasNext());
        it.next();
        assertEquals("LinkedBinaryTree.iterator()", false, it.hasNext());
    }

    private static void nodesTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        tree.addLeft(tree.root(), "left");
        tree.addRight(tree.root(), "right");

        StringBuilder sb = new StringBuilder();
        for (Node<String> node : tree.nodes()) {
            sb.append(node.getElement());
        }
        assertEquals("LinkedBinaryTree.nodes()", "rootleftright", sb.toString());
    }

    private static void siblingTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "left");
        Node<String> right = tree.addRight(tree.root(), "right");

        assertEquals("LinkedBinaryTree.sibling()", left, tree.sibling(right));
    }

    private static void childrenTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");

        StringBuilder sb = new StringBuilder();
        for (Node<String> node : tree.children(tree.root())) {
            sb.append(node.getElement());
        }

        assertEquals("LinkedBinaryTree.children()", "12", sb.toString());
    }

    private static void childrenNumberTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        assertEquals("LinkedBinaryTree.childrenNumber()", 0, tree.childrenNumber(tree.root()));
    }

    private static void isInternalTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");
        tree.addLeft(right, "22");
        assertEquals("LinkedBinaryTree.isInternal()", true, tree.isInternal(right));
        assertEquals("LinkedBinaryTree.isInternal()", false, tree.isInternal(left));
    }

    private static void isExternalTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("root");
        Node<String> left = tree.addLeft(tree.root(), "1");
        Node<String> right = tree.addRight(tree.root(), "2");
        tree.addLeft(right, "22");
        assertEquals("LinkedBinaryTree.isExternal()", false, tree.isExternal(right));
        assertEquals("LinkedBinaryTree.isExternal()", true, tree.isExternal(left));
    }

    private static void isRootTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> r = tree.addRoot("root");
        assertEquals("LinkedBinaryTree.isRoot()", true, tree.isRoot(r));
    }

    private static void isEmptyTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> r = tree.addRoot("root");
        assertEquals("LinkedBinaryTree.isEmpty()", false, tree.isEmpty());
    }

    private static void orderTest() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        tree.addRoot("F");
        Node<String> b = tree.addLeft(tree.root(), "B");
        Node<String> g = tree.addRight(tree.root(), "G");

        Node<String> a = tree.addLeft(b, "A");
        Node<String> d = tree.addRight(b, "D");
        tree.addLeft(d, "C");
        tree.addRight(d, "E");

        Node<String> i = tree.addRight(g, "I");
        tree.addLeft(i, "H");

        StringBuilder sb = new StringBuilder();

        // pre order
        Iterable<Node<String>> pre = tree.preOrder();
        for (Node<String> s : pre) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.preOrder()", "FBADCEGIH", sb.toString());
        sb.replace(0, sb.length(), "");

        // in order
        Iterable<Node<String>> in = tree.inOrder();
        for (Node<String> s : in) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.inOrder()", "ABCDEFGHI", sb.toString());
        sb.replace(0, sb.length(), "");

        // post order
        Iterable<Node<String>> post = tree.postOrder();
        for (Node<String> s : post) {
            sb.append(s.getElement());
        }
        assertEquals("LinkedBinaryTree.postOrder()", "ACEDBHIGF", sb.toString());
    }
}
