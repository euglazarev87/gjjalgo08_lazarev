package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

public class EquivalenceRelationTest {

    public static void main(String[] args) {
        MatrixHashMap.Key x = new MatrixHashMap.Key(1, 2);
        MatrixHashMap.Key y = new MatrixHashMap.Key(1, 2);
        MatrixHashMap.Key z = new MatrixHashMap.Key(1, 2);

        System.out.println("Reflexive - " + x.equals(x));
        System.out.println("Symmetric - " + Boolean.toString(x.equals(y) && y.equals(x)));
        System.out.println("Transitive - " + Boolean.toString(x.equals(y) && y.equals(z) && x.equals(z)));
        System.out.println("Consistent - " + Boolean.toString(x.equals(y) && x.equals(y) && x.equals(y)));
        System.out.println("null test - " + Boolean.toString(!x.equals(null)));
    }
}
