package main.java.com.getjavajob.training.algo08.lazareve.util;

public class StopWatch {

    private long time;

    public long start() {
        time = System.currentTimeMillis();
        return time;
    }

    public long getElapsedTime() {
        return System.currentTimeMillis() - time;
    }
}
