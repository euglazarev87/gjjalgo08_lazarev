package main.java.com.getjavajob.training.algo08.lazareve.lesson09;

import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class NavigableMapTest {

    private static final NavigableMap<Integer, String> TREE_MAP;
    private static final NavigableMap<Integer, String> EMPTY_MAP;

    static {
        TREE_MAP = new TreeMap<>();
        TREE_MAP.put(1, "Alex");
        TREE_MAP.put(2, "Boris");
        TREE_MAP.put(3, "Calwin");
        TREE_MAP.put(4, "Denis");
        TREE_MAP.put(5, "Fedor");
        TREE_MAP.put(6, "Nikolai");
        EMPTY_MAP = new TreeMap<>();
    }

    public static void main(String[] args) {
        lowerEntryTest();
        lowerKeyTest();
        floorEntryTest();
        floorKeyTest();
        ceilingEntryTest();
        ceilingKeyTest();
        higherEntryTest();
        higherKeyTest();
        firstEntryTest();
        lastEntryTest();
        pollFirstEntryTest();
        pollLastEntryTest();
        descendingMapTest();
        navigableKeySetTest();
        descendingKeySetTest();
    }

    private static void lowerEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.lowerEntry()", TREE_MAP.firstEntry(), map.lowerEntry(2));
        assertEquals("NavigableMap.lowerEntry()", true, map.lowerEntry(1) == null);
        try {
            map.lowerEntry(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.lowerEntry()", true, e instanceof NullPointerException);
        }
    }

    private static void lowerKeyTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.lowerKey()", TREE_MAP.firstKey(), map.lowerKey(2));
        assertEquals("NavigableMap.lowerKey()", true, map.lowerKey(1) == null);
        try {
            map.lowerKey(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.lowerKey()", true, e instanceof NullPointerException);
        }
    }

    private static void floorEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.floorEntry()", TREE_MAP.firstEntry(), map.floorEntry(1));
        assertEquals("NavigableMap.floorEntry()", TREE_MAP.lastEntry(), map.floorEntry(10));

        try {
            map.floorEntry(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.floorEntry()", true, e instanceof NullPointerException);
        }
    }

    private static void floorKeyTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.floorKey()", TREE_MAP.firstKey(), map.floorKey(1));
        assertEquals("NavigableMap.floorKey()", true, map.floorKey(0) == null);
        assertEquals("NavigableMap.floorKey()", map.lastKey(), map.floorKey(100500));
        try {
            map.floorKey(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.floorKey()", true, e instanceof NullPointerException);
        }
    }

    private static void ceilingEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.ceilingEntry()", TREE_MAP.firstEntry(), map.ceilingEntry(1));
        assertEquals("NavigableMap.ceilingEntry()", TREE_MAP.firstEntry(), map.ceilingEntry(0));
        assertEquals("NavigableMap.ceilingEntry()", true, map.ceilingEntry(7) == null);

        try {
            map.ceilingEntry(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.ceilingEntry()", true, e instanceof NullPointerException);
        }
    }

    private static void ceilingKeyTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.ceilingKey()", TREE_MAP.firstKey(), map.ceilingKey(1));
        assertEquals("NavigableMap.ceilingKey()", map.firstKey(), map.ceilingKey(0));
        assertEquals("NavigableMap.ceilingKey()", true, map.ceilingKey(100500) == null);
        try {
            map.ceilingKey(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.ceilingKey()", true, e instanceof NullPointerException);
        }
    }

    private static void higherEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.higherEntry()", TREE_MAP.firstEntry(), map.higherEntry(0));
        assertEquals("NavigableMap.higherEntry()", true, map.higherEntry(6) == null);

        try {
            map.higherEntry(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.higherEntry()", true, e instanceof NullPointerException);
        }
    }

    private static void higherKeyTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.higherKey()", TREE_MAP.firstKey(), map.higherKey(0));
        assertEquals("NavigableMap.higherKey()", true, map.higherKey(6) == null);

        try {
            map.higherKey(null);
            fail();
        } catch (Throwable e) {
            assertEquals("NavigableMap.higherKey()", true, e instanceof NullPointerException);
        }
    }

    private static void firstEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.firstEntry()", map.ceilingEntry(1), map.firstEntry());
        assertEquals("NavigableMap.firstEntry()", true, EMPTY_MAP.firstEntry() == null);
    }

    private static void lastEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.lastEntry()", map.ceilingEntry(6), map.lastEntry());
        assertEquals("NavigableMap.lastEntry()", true, EMPTY_MAP.lastEntry() == null);
    }

    private static void pollFirstEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.pollFirstEntry()", TREE_MAP.firstEntry(), map.pollFirstEntry());
        assertEquals("NavigableMap.pollFirstEntry()", true, EMPTY_MAP.pollFirstEntry() == null);
        assertEquals("NavigableMap.pollFirstEntry()", 5, map.size());
    }

    private static void pollLastEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        assertEquals("NavigableMap.pollLastEntry()", TREE_MAP.lastEntry(), map.pollLastEntry());
        assertEquals("NavigableMap.pollLastEntry()", 5, map.size());
        assertEquals("NavigableMap.pollLastEntry()", true, EMPTY_MAP.pollLastEntry() == null);
    }

    private static void descendingMapTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        NavigableMap<Integer, String> dmap = map.descendingMap();
        assertEquals("NavigableMap.descendingMap()", TREE_MAP.lastEntry(), dmap.firstEntry());
        assertEquals("NavigableMap.descendingMap()", map.size(), dmap.size());
        assertEquals("NavigableMap.descendingMap()", TREE_MAP.firstEntry(), dmap.lastEntry());
    }

    private static void navigableKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        NavigableSet<Integer> kset = map.navigableKeySet();
        assertEquals("NavigableMap.navigableKeySet()", TREE_MAP.firstKey(), kset.first());
        assertEquals("NavigableMap.navigableKeySet()", map.size(), kset.size());
        assertEquals("NavigableMap.navigableKeySet()", TREE_MAP.lastKey(), kset.last());
    }

    private static void descendingKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>(TREE_MAP);
        NavigableSet<Integer> kset = map.descendingKeySet();
        assertEquals("NavigableMap.descendingKeySet()", TREE_MAP.firstKey(), kset.last());
        assertEquals("NavigableMap.descendingKeySet()", map.size(), kset.size());
        assertEquals("NavigableMap.descendingKeySet()", TREE_MAP.lastKey(), kset.first());
    }
}
