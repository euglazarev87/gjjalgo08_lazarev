package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import main.java.com.getjavajob.training.algo08.lazareve.util.Assert;

import java.util.Queue;

public class LinkedListQueueTest {

    public static void main(String[] args) {
        addRemoveTest();
    }

    private static void addRemoveTest() {
        Queue<String> queue = new LinkedListQueue<>();
        queue.add("a");
        queue.add("b");
        queue.add("c");
        queue.add("d");
        queue.add("f");

        Assert.assertEquals("LinkedListQueue.add()", "a", queue.remove());
        Assert.assertEquals("LinkedListQueue.add()", "b", queue.remove());
    }
}
