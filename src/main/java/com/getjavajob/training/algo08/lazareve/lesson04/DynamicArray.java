package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;

public class DynamicArray<T> extends AbstractList<T> {

    private static final Object[] EMPTY_ARRAY = {};
    private static final int DEFAULT_SIZE = 10;

    private Object[] data;
    private int size;
    private int modCount;

    public DynamicArray() {
        data = EMPTY_ARRAY;
    }

    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new ArrayIndexOutOfBoundsException("Invalid capacity " + capacity);
        }
        data = new Object[capacity];
    }

    public boolean add(T object) {
        checkCapacity(size + 1);
        data[size++] = object;
        return true;
    }

    public void add(int index, T object) {
        checkDataRange(index);
        checkCapacity(size + 1);

        System.arraycopy(data, index, data, index + 1, size - index);

        data[index] = object;
        size++;
    }

    public T set(int index, T object) {
        checkDataRange(index);
        T prevObj = (T) data[index];
        data[index] = object;
        modCount++;
        return prevObj;
    }

    public T get(int index) {
        checkDataRange(index);
        return (T) data[index];
    }

    public T remove(int index) {
        checkDataRange(index);
        T removedObj = (T) data[index];
        System.arraycopy(data, index + 1, data, index, size - index);
        size--;
        modCount++;
        return removedObj;
    }

    public boolean remove(Object o) {
        boolean result = false;
        int index = indexOf(o);
        if (index > 0) {
            remove(index);
            result = true;
            modCount++;
        }

        return result;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (data[i] == o) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (data[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return new ListIteratorImpl();
    }

    public boolean contains(Object o) {
        return indexOf(o) > 0;
    }

    @Override
    public Iterator iterator() {
        return new ListIteratorImpl();
    }

    @Override
    public T[] toArray() {
        return (T[]) Arrays.copyOfRange(data, 0, size);
    }

    @Override
    public Object[] toArray(Object[] a) {
        return a;
    }

    private void checkCapacity(int size) {
        int minCapacity = Math.max(size, DEFAULT_SIZE);
        if (minCapacity - data.length > 0) {
            increaseData(minCapacity);
        }
        modCount++;
    }

    private void increaseData(int capacity) {
        int oldCapacity = data.length;
        int newCapacity = oldCapacity + (oldCapacity / 2);
        if (newCapacity <= 0) {
            newCapacity = capacity;
        }
        data = Arrays.copyOf(data, newCapacity);
    }

    private void checkDataRange(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    @Override
    public String toString() {
        return Arrays.deepToString(data);
    }

    @Override
    public ListIterator listIterator(int index) {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl implements ListIterator<T> {

        private int cursor;
        private int expectedModCount = modCount;

        public boolean hasNext() {
            checkForComodification();
            return cursor != size;
        }

        public T next() {
            checkForComodification();
            checkDataRange(cursor);
            return (T) data[cursor++];
        }

        public boolean hasPrevious() {
            checkForComodification();
            return cursor != 0;
        }

        public T previous() {
            checkForComodification();
            checkDataRange(cursor - 1);
            return (T) data[--cursor];
        }

        public int nextIndex() {
            checkForComodification();
            return cursor;
        }

        public int previousIndex() {
            checkForComodification();
            return cursor - 1;
        }

        public void remove() {
            checkForComodification();
            DynamicArray.this.remove(cursor--);
            expectedModCount++;
        }

        public void set(T o) {
            checkForComodification();
            checkDataRange(cursor + 1);
            data[cursor++] = o;
            expectedModCount++;
        }

        public void add(T o) {
            checkForComodification();
            checkDataRange(cursor + 1);
            DynamicArray.this.add(cursor++, o);
            expectedModCount++;
        }

        final private void checkForComodification() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
