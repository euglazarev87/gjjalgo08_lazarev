package main.java.com.getjavajob.training.algo08.lazareve.lesson10;

public class BubbleSort {

    public static <T extends Comparable<T>> void sort(T[] list) {
        boolean swapped = true;
        int k = 0;
        while (swapped) {
            swapped = false;
            for (int i = 1; i < list.length - k; i++) {
                if (list[i - 1].compareTo(list[i]) > 0) {
                    swap(list, i - 1, i);
                    swapped = true;
                }
            }
            k++;
        }
    }

    private static <T> void swap(T[] list, int a, int b) {
        T temp = list[a];
        list[a] = list[b];
        list[b] = temp;
    }
}
