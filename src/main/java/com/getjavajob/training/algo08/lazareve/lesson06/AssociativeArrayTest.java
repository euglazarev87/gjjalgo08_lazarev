package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.nullAssertEquals;

public class AssociativeArrayTest {

    public static void main(String[] args) {
        addTest();
        getTest();
        removeTest();
    }

    private static void removeTest() {
        IAssocArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("2", 2);
        aa.add("3", 3);
        aa.add("4", 4);
        aa.add("2", 5);

        assertEquals("AssociativeArray.remove()", 1, aa.remove("1"));
        assertEquals("AssociativeArray.remove()", 5, aa.remove("2"));
        nullAssertEquals("AssociativeArray.remove()", aa.remove("6"));
    }

    private static void getTest() {
        IAssocArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("2", 2);
        aa.add("3", 3);
        aa.add("4", 4);
        aa.add("2", 5);
        assertEquals("AssociativeArray.get()", 5, aa.get("2"));
        assertEquals("AssociativeArray.get()", 1, aa.get("1"));
    }

    private static void addTest() {
        IAssocArray<String, Integer> aa = new AssociativeArray<>();
        nullAssertEquals("AssociativeArray.add()", aa.add("1", 1));
        assertEquals("AssociativeArray.add()", 1, aa.add("1", 2));
    }
}
