package main.java.com.getjavajob.training.algo08.lazareve.util;

public class PerformanceTestResult {
    public String className;
    public long addResult;
    public long removeResult;

    public PerformanceTestResult(String name) {
        className = name;
    }
}
