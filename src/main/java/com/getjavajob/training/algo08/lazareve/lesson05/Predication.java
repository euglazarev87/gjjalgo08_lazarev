package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public interface Predication<T> {
    boolean apply(T el);
}
