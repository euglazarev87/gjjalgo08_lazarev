package main.java.com.getjavajob.training.algo08.lazareve.util;

import java.util.List;

public class ListPerformanceTest {

    private static int DEFAULT_TEST_SIZE = 10699567;
    private String resultFileName;
    private int testSize;
    private PerformanceTestResult resultOne;
    private PerformanceTestResult resultTwo;
    private List<String> subjectOne;
    private List<String> subjectTwo;
    private StopWatch watcher;

    public ListPerformanceTest(int sizeOfData, String fileName, List<String> subject1, List<String> subject2) {
        resultFileName = fileName;
        testSize = sizeOfData;
        subjectOne = subject1;
        subjectTwo = subject2;
        watcher = new StopWatch();
        resultOne = new PerformanceTestResult(subject1.getClass().getName());
        resultTwo = new PerformanceTestResult(subject2.getClass().getName());
    }

    public ListPerformanceTest(String fileName, List<String> subject1, List<String> subject2) {
        this(DEFAULT_TEST_SIZE, fileName, subject1, subject2);
    }

    public static int getDefaultTestSize() {
        return DEFAULT_TEST_SIZE;
    }

    public static void setDefaultTestSize(int defaultTestSize) {
        DEFAULT_TEST_SIZE = defaultTestSize;
    }

    public String getResultFileName() {
        return resultFileName;
    }

    public void setResultFileName(String resultFileName) {
        this.resultFileName = resultFileName;
    }

    public int getTestSize() {
        return testSize;
    }

    public void setTestSize(int testSize) {
        this.testSize = testSize;
    }

    public PerformanceTestResult getResultOne() {
        return resultOne;
    }

    public void setResultOne(PerformanceTestResult resultOne) {
        this.resultOne = resultOne;
    }

    public PerformanceTestResult getResultTwo() {
        return resultTwo;
    }

    public void setResultTwo(PerformanceTestResult resultTwo) {
        this.resultTwo = resultTwo;
    }

    public List<String> getSubjectOne() {
        return subjectOne;
    }

    public void setSubjectOne(List<String> subjectOne) {
        this.subjectOne = subjectOne;
    }

    public List<String> getSubjectTwo() {
        return subjectTwo;
    }

    public void setSubjectTwo(List<String> subjectTwo) {
        this.subjectTwo = subjectTwo;
    }

    public void doPerformanceTest() {
        StringBuilder sb = new StringBuilder();
        sb.append(beginTest());
        sb.append(middleTest());
        sb.append(endTest());
        Helper.writeToFile(resultFileName, sb.toString());
        System.out.println("Performance test finished");
    }

    protected String beginTest() {
        List<String> firstList = getTemplate(subjectOne);
        List<String> secondList = getTemplate(subjectTwo);

        watcher.start();
        firstList.add(0, "999");
        resultOne.addResult = watcher.getElapsedTime();

        watcher.start();
        secondList.add(0, "999");
        resultTwo.addResult = watcher.getElapsedTime();

        watcher.start();
        firstList.remove(0);
        resultTwo.removeResult = watcher.getElapsedTime();

        watcher.start();
        secondList.remove(0);
        resultTwo.removeResult = watcher.getElapsedTime();

        return formatResult("Begin test");
    }

    protected String middleTest() {
        List<String> firstList = getTemplate(subjectOne);
        List<String> secondList = getTemplate(subjectTwo);

        watcher.start();
        firstList.add(testSize / 2, "999");
        resultOne.addResult = watcher.getElapsedTime();

        watcher.start();
        secondList.add(testSize / 2, "999");
        resultTwo.addResult = watcher.getElapsedTime();

        watcher.start();
        firstList.remove(testSize / 2);
        resultOne.removeResult = watcher.getElapsedTime();

        watcher.start();
        secondList.remove(testSize / 2);
        resultTwo.removeResult = watcher.getElapsedTime();

        return formatResult("Middle test");
    }

    protected String endTest() {
        List<String> firstList = getTemplate(subjectOne);
        List<String> secondList = getTemplate(subjectTwo);

        watcher.start();
        firstList.add("999");
        resultOne.addResult = watcher.getElapsedTime();

        watcher.start();
        secondList.add("999");
        resultTwo.addResult = watcher.getElapsedTime();

        watcher.start();
        firstList.remove(testSize);
        resultOne.removeResult = watcher.getElapsedTime();

        watcher.start();
        secondList.remove(testSize);
        resultTwo.removeResult = watcher.getElapsedTime();

        return formatResult("End test");
    }

    protected String formatResult(String methodName) {

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(methodName + "\n");
        sb.append("==============================\n");
        sb.append("ADD\n");
        sb.append(resultOne.className + " " + resultOne.addResult + " ms\n");
        sb.append(resultTwo.className + " " + resultTwo.addResult + " ms\n");
        sb.append("-----------\n");
        sb.append("REMOVE\n");
        sb.append(resultOne.className + " " + resultOne.removeResult + " ms\n");
        sb.append(resultTwo.className + " " + resultTwo.removeResult + " ms\n");
        sb.append("==============================\n");

        return sb.toString();
    }

    protected List<String> getTemplate(List<String> collection) {
        for (int i = 1; i <= testSize; i++) {
            collection.add(Integer.toString(i));
        }
        return collection;
    }
}
