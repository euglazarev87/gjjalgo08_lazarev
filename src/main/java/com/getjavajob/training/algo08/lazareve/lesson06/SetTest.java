package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import java.util.HashSet;
import java.util.Set;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class SetTest {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        Set<String> set = new HashSet<>();
        set.add("a");
        set.add("a");
        assertEquals("Set.add()", 1, set.size());
        set.add("b");
        Set<String> newSet = new HashSet<>();
        newSet.addAll(set);
        assertEquals("Set.addAll()", 2, newSet.size());
    }
}
