package main.java.com.getjavajob.training.algo08.lazareve.lesson03;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.fail;

public class DynamicArrayTest {

    public static void main(String[] args) {
        addToTheBegin();
        addToTheMiddle();
        addToTheEnd();
        setTest();
        getTest();
        removeFromTheBegin();
        removeFromTheMiddle();
        removeFromTheEnd();
        removeObjectFromTheBegin();
        sizeTest();
        containsTest();
        indexOfTest();
        checkRangeTest();
    }

    private static void addToTheBegin() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);
        source.add(0, 999);

        Object[] example = {999, 1, 2, 3};
        assertEquals("DynamicArray.add to the begin", example, source.toArray());
    }

    private static void addToTheMiddle() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);
        source.add(2, 999);

        Object[] example = {1, 2, 999, 3};
        assertEquals("DynamicArray.add to the middle", example, source.toArray());
    }

    private static void addToTheEnd() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);
        source.add(999);

        Object[] example = {1, 2, 3, 999};
        assertEquals("DynamicArray.add to the end", example, source.toArray());
    }

    private static void setTest() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);

        Object obj = source.set(0, 999);
        assertEquals("DynamicArray.set", 1, (int) obj);
    }

    private static void getTest() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);

        assertEquals("DynamicArray.get", 1, (int) source.get(0));
    }

    private static void removeFromTheBegin() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);

        Object[] arr = {2, 3};

        assertEquals("DynamicArray.remove:begin", 1, (int) source.remove(0));
        assertEquals("DynamicArray.remove:begin", arr, source.toArray());
    }

    private static void removeFromTheMiddle() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);

        Object[] arr = {1, 3};

        assertEquals("DynamicArray.remove:middle", 2, (int) source.remove(1));
        assertEquals("DynamicArray.remove:middle", arr, source.toArray());
    }

    private static void removeFromTheEnd() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(3);

        Object[] arr = {1, 3};

        assertEquals("DynamicArray.remove:end", 2, (int) source.remove(1));
        assertEquals("DynamicArray.remove:end", arr, source.toArray());
    }

    private static void removeObjectFromTheBegin() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(99);
        source.add(3);
        source.add(99);

        Object[] arr = {1, 2, 3, 99};
        source.remove(Integer.valueOf(99));
        assertEquals("DynamicArray.remove object", arr, source.toArray());
    }

    private static void indexOfTest() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(99);
        source.add(3);
        source.add(99);

        assertEquals("DynamicArray.indexOf", 2, source.indexOf(99));
    }

    private static void sizeTest() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(99);

        assertEquals("DynamicArray.size", 3, source.size());
    }

    private static void containsTest() {
        DynamicArray source = new DynamicArray();
        source.add(1);
        source.add(2);
        source.add(99);
        assertEquals("DynamicArray.contains", true, source.contains(99));
        assertEquals("DynamicArray.contains", false, source.contains(88));
    }

    private static void checkRangeTest() {
        DynamicArray source = new DynamicArray();
        try {
            source.get(1);
            fail();
        } catch (Throwable e) {
            assertEquals("DynamicArray.checkDataRange", true, e instanceof ArrayIndexOutOfBoundsException);
        }
    }
}
