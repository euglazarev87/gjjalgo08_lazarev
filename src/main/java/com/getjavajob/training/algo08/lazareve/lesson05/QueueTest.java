package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class QueueTest {

    private static final Queue<Integer> INTEGER_LIST;
    private static final Queue<String> STRING_LIST;

    static {
        INTEGER_LIST = new ArrayDeque<>();
        INTEGER_LIST.add(1);
        INTEGER_LIST.add(2);
        INTEGER_LIST.add(3);
        INTEGER_LIST.add(4);
        INTEGER_LIST.add(5);
        INTEGER_LIST.add(6);
        INTEGER_LIST.add(7);
        INTEGER_LIST.add(8);
        INTEGER_LIST.add(9);
        INTEGER_LIST.add(10);

        STRING_LIST = new ArrayDeque<>();
        STRING_LIST.add("1");
        STRING_LIST.add("2");
        STRING_LIST.add("3");
        STRING_LIST.add("4");
        STRING_LIST.add("5");
        STRING_LIST.add("6");
        STRING_LIST.add("7");
        STRING_LIST.add("8");
        STRING_LIST.add("9");
        STRING_LIST.add("10");
    }

    public static void main(String[] args) {
        sizeTest();
        isEmptyTest();
        containsTest();
        toArrayTest();
        addTest();
        removeTest();
        addAllTest();
        clearTest();
        retainAllTest();
        removeAllTest();
        containsAllTest();
        offerTest();
        pollTest();
        elementTest();
        peekTest();
    }

    private static void sizeTest() {
        Queue<Integer> list = new ArrayDeque<>(INTEGER_LIST);
        assertEquals("ArrayDeque.size()", 10, list.size());
    }

    private static void isEmptyTest() {
        Queue<Integer> list = new ArrayDeque<>(INTEGER_LIST);
        assertEquals("ArrayDeque.isEmpty()", false, list.isEmpty());
    }

    private static void containsTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.contains()", false, list.contains("0000"));
    }

    private static void toArrayTest() {

        Queue<Human> list = new ArrayDeque<>();
        Human pers1 = new Human("Eugene");
        Human pers2 = new Human("Katrin");
        Human pers3 = new Human("Eva");

        Human[] arr = new Human[3];
        arr[0] = pers1;
        arr[1] = pers2;
        arr[2] = pers3;

        list.add(pers1);
        list.add(pers2);
        list.add(pers3);

        assertEquals("ArrayDeque.toArray()", arr, list.toArray());
        assertEquals("ArrayDeque.toArray()", arr, list.toArray(new Human[0]));
    }

    private static void addTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        list.add("Learn Java");
        assertEquals("ArrayDeque.add()", true, list.contains("Learn Java"));
    }

    private static void removeTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        list.add("Learn Java");
        list.remove("Learn Java");
        assertEquals("ArrayDeque.remove()", false, list.contains("Learn Java"));
    }

    private static void addAllTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        Queue<String> testList = new ArrayDeque<>();
        testList.addAll(STRING_LIST);
        assertEquals("ArrayDeque.addAll()", list.toArray(), testList.toArray());
    }

    private static void clearTest() {
        Queue<Integer> list = new ArrayDeque<>(INTEGER_LIST);
        list.clear();
        assertEquals("ArrayDeque.clear()", true, list.isEmpty());
    }

    private static void retainAllTest() {
        Queue<Integer> list = new ArrayDeque<>(INTEGER_LIST);
        list.add(1);
        Queue<Integer> retList = new ArrayDeque<>();
        retList.add(1);
        list.retainAll(retList);
        assertEquals("ArrayDeque.retainAll()", new Integer[]{1, 1}, list.toArray(new Integer[0]));
    }

    private static void removeAllTest() {
        Queue<Integer> list = new ArrayDeque<>();
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(2);

        Queue<Integer> delList = new ArrayDeque<>();
        delList.add(2);
        list.removeAll(delList);
        assertEquals("ArrayDeque.removeAll()", new Integer[]{1}, list.toArray(new Integer[0]));
    }

    private static void containsAllTest() {
        Queue<Integer> list = new ArrayDeque<>();
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(2);

        Queue<Integer> containList = new ArrayDeque<>();
        containList.add(1);

        Queue<Integer> notContainList = new ArrayDeque<>();
        notContainList.add(1);
        notContainList.add(3);

        assertEquals("ArrayDeque.containsAll()", true, list.containsAll(containList));
        assertEquals("ArrayDeque.containsAll()", false, list.containsAll(notContainList));
    }

    private static void offerTest() {
        Queue<Integer> list = new ArrayDeque<>();
        list.add(1);
        list.add(2);
        list.offer(9999);
        assertEquals("ArrayDeque.offer()", true, list.contains(9999));
    }

    private static void pollTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.pool()", "1", list.poll());
        assertEquals("ArrayDeque.pool()", false, list.contains("1"));
    }

    private static void elementTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.element()", "1", list.element());
        assertEquals("ArrayDeque.element()", true, list.contains("1"));
    }

    private static void peekTest() {
        Queue<String> list = new ArrayDeque<>(STRING_LIST);
        assertEquals("ArrayDeque.peek()", "1", list.peek());
        assertEquals("ArrayDeque.peek()", true, list.contains("1"));
    }

    static class Human {
        String name;

        Human(String name) {
            this.name = name;
        }
    }
}
