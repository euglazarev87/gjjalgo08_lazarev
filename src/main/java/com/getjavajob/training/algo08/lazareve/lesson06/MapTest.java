package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import java.util.*;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.nullAssertEquals;

public class MapTest {

    private static final Map<String, String> STRING_MAP;

    static {
        STRING_MAP = new HashMap<>();
        STRING_MAP.put("First", "First");
        STRING_MAP.put("Second", "Second");
        STRING_MAP.put("Third", "Third");
        Collections.unmodifiableMap(STRING_MAP);
    }

    public static void main(String[] args) {
        sizeTest();
        isEmptyTest();
        containsKeyTest();
        containsValueTest();
        getTest();
        putTest();
        removeTest();
        putAllTest();
        clearTest();
        keySetTest();
        valuesTest();
        entrySet();
    }

    private static void sizeTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.size()", 3, map.size());
    }

    private static void isEmptyTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.isEmpty()", false, map.isEmpty());
    }

    private static void containsKeyTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.containsKey()", false, map.containsKey("Fourth"));
        assertEquals("Map.containsKey()", true, map.containsKey("First"));
    }

    private static void containsValueTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.containsValue()", false, map.containsKey("Fourth"));
        assertEquals("Map.containsValue()", true, map.containsKey("First"));
    }

    private static void getTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.get()", "Second", map.get("Second"));
        nullAssertEquals("Map.get()", map.get("S"));
    }

    private static void putTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        nullAssertEquals("Map.put()", map.put("S", "S"));
        assertEquals("Map.put()", "S", map.put("S", "P"));
    }

    private static void removeTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        nullAssertEquals("Map.remove()", map.remove("S"));
        assertEquals("Map.remove()", "Second", map.remove("Second"));
    }

    private static void putAllTest() {
        Map<String, String> map1 = new HashMap<>(STRING_MAP);
        Map<String, String> map2 = new HashMap<>();
        map2.putAll(STRING_MAP);

        assertEquals("Map.putAll()", true, map1.equals(map2));
    }

    private static void clearTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        map.clear();

        assertEquals("Map.clear()", true, map.isEmpty());
    }

    private static void keySetTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);

        Set<String> set = new HashSet<>();
        set.add("First");
        set.add("Second");
        set.add("Third");

        assertEquals("Map.keySet()", true, map.keySet().equals(set));
    }

    private static void valuesTest() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        List<String> set = new ArrayList<>();
        set.add("First");
        set.add("Second");
        set.add("Third");
        assertEquals("Map.values()", true, map.values().containsAll(set));
    }

    private static void entrySet() {
        Map<String, String> map = new HashMap<>(STRING_MAP);
        assertEquals("Map.entrySet()", 3, map.entrySet().size());
    }
}
