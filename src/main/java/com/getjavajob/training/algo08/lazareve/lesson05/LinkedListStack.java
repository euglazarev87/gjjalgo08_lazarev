package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> list;

    public LinkedListStack() {
        list = new SinglyLinkedList();
    }

    @Override
    public void push(E e) {
        list.head = new SinglyLinkedList.Node<>(e, list.head);
    }

    @Override
    public E pop() {
        return list.pop();
    }
}
