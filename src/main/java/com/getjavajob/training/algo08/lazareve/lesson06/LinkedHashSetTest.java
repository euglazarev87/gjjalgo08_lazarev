package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class LinkedHashSetTest {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        Set<String> set = new LinkedHashSet<>();
        set.add("First");
        set.add("Second");
        set.add("Third");
        set.add("Fourth");
        set.add("Fifth");

        Iterator<String> iterator = set.iterator();
        assertEquals("LinkedHashSet.add()", "First", iterator.next());
        assertEquals("LinkedHashSet.add()", "Second", iterator.next());
        assertEquals("LinkedHashSet.add()", "Third", iterator.next());
        assertEquals("LinkedHashSet.add()", "Fourth", iterator.next());
        assertEquals("LinkedHashSet.add()", "Fifth", iterator.next());
    }
}
