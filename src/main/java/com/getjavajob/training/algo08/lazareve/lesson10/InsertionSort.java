package main.java.com.getjavajob.training.algo08.lazareve.lesson10;

public class InsertionSort {

    public static <T extends Comparable<T>> void sort(T[] list) {
        int j = 0;
        for (int i = 1; i < list.length; i++) {
            j = i;
            while (j > 0 && list[j - 1].compareTo(list[j]) > 0) {
                swap(list, j - 1, j);
                j--;
            }
        }
    }

    private static <T> void swap(T[] list, int i, int j) {
        T temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }

}
