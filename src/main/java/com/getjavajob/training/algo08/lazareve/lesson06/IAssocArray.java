package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

public interface IAssocArray<K, V> {

    V add(K key, V value);

    V get(K key);

    V remove(K key);
}
