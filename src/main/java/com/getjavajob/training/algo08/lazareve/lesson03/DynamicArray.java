package main.java.com.getjavajob.training.algo08.lazareve.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;

public class DynamicArray {

    private static final Object[] EMPTY_ARRAY = {};
    private static final int DEFAULT_SIZE = 10;

    private Object[] data;
    private int size;
    private int modCount;

    public DynamicArray() {
        data = EMPTY_ARRAY;
    }

    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new ArrayIndexOutOfBoundsException("Invalid capacity " + capacity);
        }
        data = new Object[capacity];
        this.size = 0;
    }

    public boolean add(Object object) {
        checkCapacity(size + 1);
        data[size++] = object;
        modCount++;
        return true;
    }

    public void add(int index, Object object) {
        checkDataRange(index);
        checkCapacity(size + 1);

        System.arraycopy(data, index, data, index + 1, size - index);

        data[index] = object;
        size++;
        modCount++;
    }

    public Object set(int index, Object object) {
        checkDataRange(index);
        Object prevObj = data[index];
        data[index] = object;
        modCount++;
        return prevObj;
    }

    public Object get(int index) {
        checkDataRange(index);
        return data[index];
    }

    public Object remove(int index) {
        checkDataRange(index);
        Object removedObj = data[index];
        System.arraycopy(data, index + 1, data, index, size - index);
        size--;
        modCount++;
        return removedObj;
    }

    public boolean remove(Object o) {
        boolean result = false;
        int index = indexOf(o);
        if (index > 0) {
            remove(index);
            result = true;
        }
        modCount++;

        return result;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object o) {

        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (data[i] == o) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (data[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object o) {
        return indexOf(o) > 0;
    }

    public Object[] toArray() {
        return Arrays.copyOfRange(data, 0, size);
    }

    private void checkCapacity(int size) {
        int minCapacity = Math.max(size, DEFAULT_SIZE);
        if (minCapacity - data.length > 0) {
            increaseData(minCapacity);
        }
    }

    private void increaseData(int capacity) {
        int oldCapacity = data.length;
        int newCapacity = oldCapacity + (oldCapacity / 2);
        if (newCapacity <= 0) {
            newCapacity = capacity;
        }
        data = Arrays.copyOf(data, newCapacity);
    }

    private void checkDataRange(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    @Override
    public String toString() {
        return Arrays.deepToString(data);
    }

    public ListIterator listIterator() {
        return new ListIterator();
    }

    private class ListIterator {

        private int cursor;
        int expectedModCount = modCount;

        public boolean hasNext() {
            checkForComodification();
            return cursor != size;
        }

        public Object next() {
            checkForComodification();
            checkDataRange(cursor);
            return data[cursor++];
        }

        public boolean hasPrevious() {
            checkForComodification();
            return cursor != 0;
        }

        public Object previous() {
            checkForComodification();
            checkDataRange(cursor - 1);
            return data[--cursor];
        }

        public int nextIndex() {
            checkForComodification();
            return cursor;
        }

        public int previousIndex() {
            checkForComodification();
            return cursor - 1;
        }

        public void remove() {
            checkForComodification();
            DynamicArray.this.remove(cursor--);
            expectedModCount++;
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }
}
