package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

public interface Stack<E> {
    void push(E e);

    E pop();
}
