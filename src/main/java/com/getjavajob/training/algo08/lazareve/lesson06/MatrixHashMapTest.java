package main.java.com.getjavajob.training.algo08.lazareve.lesson06;

public class MatrixHashMapTest {

    private static final int TEST_SIZE = 1_000_000;

    public static void main(String[] args) {
        MatrixHashMap<Integer> m = new MatrixHashMap<>();
        int column = TEST_SIZE;
        for (int i = 0; i <= column; i++) {
            m.set(i, column, i);
        }
        System.out.println(m.get(TEST_SIZE, TEST_SIZE));

    }
}
