package main.java.com.getjavajob.training.algo08.lazareve.lesson04;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static main.java.com.getjavajob.training.algo08.lazareve.util.Assert.assertEquals;

public class CollectionListTest {

    private static final Collection<Integer> INTEGER_LIST;
    private static final Collection<String> STRING_LIST;

    static {
        INTEGER_LIST = new ArrayList<>();
        INTEGER_LIST.add(1);
        INTEGER_LIST.add(2);
        INTEGER_LIST.add(3);
        INTEGER_LIST.add(4);
        INTEGER_LIST.add(5);
        INTEGER_LIST.add(6);
        INTEGER_LIST.add(7);
        INTEGER_LIST.add(8);
        INTEGER_LIST.add(9);
        INTEGER_LIST.add(10);

        STRING_LIST = new ArrayList<>();
        STRING_LIST.add("1");
        STRING_LIST.add("2");
        STRING_LIST.add("3");
        STRING_LIST.add("4");
        STRING_LIST.add("5");
        STRING_LIST.add("6");
        STRING_LIST.add("7");
        STRING_LIST.add("8");
        STRING_LIST.add("9");
        STRING_LIST.add("10");
    }

    public static void main(String[] args) {
        sizeTest();
        isEmptyTest();
        containsTest();
        toArrayTest();
        addTest();
        removeTest();
        containsAllTest();
        addAllTest();
        removeAllTest();
        retainAllTest();
        clearTest();
        equalsTest();
        getTest();
        setTest();
        indexOfTest();
        lastIndexOfTest();
        subListTest();
    }

    private static void sizeTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        assertEquals("ArrayList.size()", 10, list.size());
    }

    private static void isEmptyTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        Collection<Integer> emList = new ArrayList<>();
        assertEquals("ArrayList.isEmpty()", false, list.isEmpty());
        assertEquals("ArrayList.isEmpty()", true, emList.isEmpty());
    }

    private static void containsTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        assertEquals("ArrayList.contains()", false, list.contains(999));
    }

    private static void toArrayTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        assertEquals("ArrayList.toArray()", 10, list.toArray().length);
    }

    private static void addTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        list.add(999);
        assertEquals("ArrayList.add()", true, list.contains(999));
    }

    private static void removeTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        list.remove("1");
        list.remove(1);
        assertEquals("ArrayList.remove()", false, list.contains("1"));
        assertEquals("ArrayList.remove()", false, list.contains("2"));
    }

    private static void containsAllTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        List<Integer> trueList = new ArrayList<>();
        trueList.add(1);
        trueList.add(2);

        List<Integer> falseList = new ArrayList<>();
        falseList.add(1);
        falseList.add(999);
        assertEquals("ArrayList.containsAllTest()", true, list.containsAll(trueList));
        assertEquals("ArrayList.containsAllTest()", false, list.containsAll(falseList));
    }

    private static void addAllTest() {
        Collection<Integer> list = new ArrayList<>();
        list.addAll(INTEGER_LIST);
        assertEquals("ArrayList.addAllTest()", true, list.containsAll(INTEGER_LIST));
    }

    private static void removeAllTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        list.removeAll(INTEGER_LIST);
        assertEquals("ArrayList.removeAllTest()", true, list.isEmpty());
    }

    private static void retainAllTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);

        List<Integer> retList = new ArrayList<>();
        retList.add(1);
        retList.add(2);
        retList.add(3);

        list.retainAll(retList);
        assertEquals("ArrayList.retainAllTest()", retList.size(), list.size());
    }

    private static void clearTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        list.clear();
        assertEquals("ArrayList.clearTest()", true, list.isEmpty());
    }

    private static void equalsTest() {
        Collection<Integer> list = new ArrayList<>(INTEGER_LIST);
        assertEquals("ArrayList.equalsTest()", true, list.equals(INTEGER_LIST));
    }

    private static void getTest() {
        List<String> list = new ArrayList<>(STRING_LIST);
        assertEquals("ArrayList.getTest()", "10", list.get(9));
    }

    private static void setTest() {
        List<String> list = new ArrayList<>(STRING_LIST);
        assertEquals("ArrayList.setTest()", "10", list.set(9, "999"));
    }

    private static void indexOfTest() {
        List<String> list = new ArrayList<>(STRING_LIST);
        assertEquals("ArrayList.indexOfTest()", 9, list.indexOf("10"));
    }

    private static void lastIndexOfTest() {
        List<String> list = new ArrayList<>(STRING_LIST);
        list.add("1");
        assertEquals("ArrayList.lastIndexOfTest()", 10, list.lastIndexOf("1"));
    }

    private static void subListTest() {
        List<String> list = new ArrayList<>(STRING_LIST);
        List<String> result = new ArrayList<>();
        result.add("1");
        result.add("2");
        result.add("3");
        assertEquals("ArrayList.subListTest()", result, list.subList(0, 3));
    }
}
