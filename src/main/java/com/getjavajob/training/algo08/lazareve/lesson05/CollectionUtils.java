package main.java.com.getjavajob.training.algo08.lazareve.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CollectionUtils {

    public static void main(String[] args) {

        Collection<String> s = new ArrayList<>();
        s.add("1");
        s.add("2");
        s.add("3");

        transformS(s, new Transformator<String, Integer>() {
            @Override
            public Integer transform(String source) {
                return Integer.parseInt(source);
            }
        });

        System.out.println(s.toString());
    }

    public static <T> Collection<T> filter(Collection<T> target, Predication<T> predicate) {
        Collection<T> list = new ArrayList<>();
        for (T element : target) {
            if (predicate.apply(element)) {
                list.add(element);
            }
        }
        return list;
    }

    public static <I, O> Collection<O> transform(Collection<I> collection, Transformator<I, O> transformator) {
        Collection<O> list = new ArrayList<>();
        for (I el : collection) {
            list.add(transformator.transform(el));
        }
        return list;
    }

    public static <I, O> Collection<O> transformS(Collection collection, Transformator<I, O> transformator) {
        Collection<I> col = new ArrayList<>(collection);
        collection.clear();
        for (I el : col) {
            collection.add(transformator.transform(el));
        }
        return collection;
    }

    public static <T> Collection<T> forAllDo(Collection<T> source, Task<T> task) {
        for (T element : source) {
            task.doTask(element);
        }
        return source;
    }

    public static <T> Collection<T> unmodifiableCollection(Collection<T> col) {
        return Collections.unmodifiableCollection(col);
    }
}
