package main.java.com.getjavajob.training.algo.tree.binary.search.balanced;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.LinkedBinaryTree;
import main.java.com.getjavajob.training.algo.tree.binary.search.BinarySearchTree;

public class BalanceableTree<E extends Comparable<E>> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(main.java.com.getjavajob.training.algo.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent new parent
     * @param child new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (parent != null) {
            LinkedBinaryTree.NodeImpl<E> curRoot = (LinkedBinaryTree.NodeImpl<E>) parent(parent);
            if (makeLeftChild) {
                parent.setRight(child.getLeft());
                child.setLeft(parent);
            } else {
                parent.setLeft(child.getRight());
                child.setRight(parent);
            }

            if (parent == root()) {
                this.root = child;
            }

            if (curRoot != null) {
                if (left(curRoot) == parent) {
                    curRoot.setLeft(child);
                } else {
                    curRoot.setRight(child);
                }
            }
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        relink((LinkedBinaryTree.NodeImpl<E>) parent(n), (LinkedBinaryTree.NodeImpl<E>) n, right(parent(n)) == n);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(main.java.com.getjavajob.training.algo.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     *
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     *
     * Similarly for subtree with right side children.
     *
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        rotate(n);
        rotate(n);
        return n;
    }

    public void reduceSubtreeTest(Node<E> n) {
        reduceSubtreeHeight(n);
    }
}
