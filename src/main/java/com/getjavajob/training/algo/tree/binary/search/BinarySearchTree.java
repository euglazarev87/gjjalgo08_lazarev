package main.java.com.getjavajob.training.algo.tree.binary.search;

import main.java.com.getjavajob.training.algo.tree.Node;
import main.java.com.getjavajob.training.algo.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {

    protected Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (comparator != null) {
            return comparator.compare(val1, val2);
        }
        return ((Comparable)val1).compareTo(val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        return searchInNode(n, val);
    }

    public Node<E> findNode(E key) {
        return getNode(key);
    }

    private Node<E> searchInNode(Node<E> n, E val) {
        NodeImpl<E> node = validate(n);
        int compareResult;
        while (node != null) {
            compareResult = compare(node.getElement(), val);
            if (val.equals(node.getElement())) {
                return node;
            }

            if (compareResult > 0) {
                node = validate(node.getLeft());
            } else {
                node = validate(node.getRight());
            }
        }

        return null;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public Node<E> add(E value) throws IllegalStateException {
        NodeImpl<E> newNode = new NodeImpl<E>(value);
        insert(validate(root()), newNode);
        size++;
        return newNode;
    }

    protected void insert(NodeImpl<E> r, NodeImpl<E> newNode) {

        if (r == null) {
            root = newNode;
            return;
        }

        if (compare(newNode.getElement(), r.getElement()) < 0) {
            if (r.getLeft() == null) {
                r.setLeft(newNode);
            } else {
                insert(validate(r.getLeft()), newNode);
            }

        } else {
            if (r.getRight() == null) {
                r.setRight(newNode);
            } else {
                insert(validate(r.getRight()), newNode);
            }
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    public void remove(E e) {
        Node<E> n = getNode(e);
        delete(n);
    }

    protected void delete(Node<E> n) {
        if (n == null) {
            throw new IllegalArgumentException("Argument is null");
        }

        int numberOfChild = childrenNumber(n);
        NodeImpl<E> node = validate(n);
        switch (numberOfChild) {
            case 0: // if no child
                NodeImpl<E> parent = validate(parent(n));
                if (parent.getLeft() == node) {
                    parent.setLeft(null);
                } else {
                    parent.setRight(null);
                }
                break;
            case 1: // if only one child
                if (node.getLeft() != null) {
                    node.setValue(node.getLeft().getElement());
                    node.setLeft(null);
                }
                break;
            case 2: // if two children
                NodeImpl<E> mostRight = node;
                NodeImpl<E> parentOfMostRight = null;
                while (mostRight != null) {
                    if (isExternal(mostRight)) {
                        break;
                    }
                    parentOfMostRight = mostRight;
                    mostRight = validate(mostRight.getRight());
                }

                node.setValue(mostRight.getElement());
                parentOfMostRight.setRight(null);

                break;
            default:
        }
        size--;
    }

    protected Node<E> getNode(E e) {
        Node<E> p = root;
        while (p != null) {
            int cmp = compare(e, p.getElement());
            if (cmp < 0) {
                p = left(p);
            } else if (cmp > 0) {
                p = right(p);
            } else {
                return p;
            }
        }
        return null;
    }

}
