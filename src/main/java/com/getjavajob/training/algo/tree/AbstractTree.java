package main.java.com.getjavajob.training.algo.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {

    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return n == root();
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Iterable<Node<E>> preOrder() {
        List<Node<E>> list = new ArrayList<>(size());
        preOrderTraversal(list, root());
        return list;
    }

    private void preOrderTraversal(List<Node<E>> list, Node<E> node) {
        if (node == null) {
            return;
        }
        list.add(node);
        for (Node<E> n : children(node)) {
            preOrderTraversal(list, n);
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Iterable<Node<E>> postOrder() {
        List<Node<E>> list = new ArrayList<>();
        postOrderTraversal(list, root());
        return list;
    }

    private void postOrderTraversal(List<Node<E>> list, Node<E> node) {
        if (node == null) {
            return;
        }
        for (Node<E> n : children(node)) {
            postOrderTraversal(list, n);
        }
        list.add(node);
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Iterable<Node<E>> breadthFirst() {
        Queue<Node<E>> q = new ArrayDeque<>();
        List<Node<E>> list = new ArrayList<>();
        Node<E> node;
        q.add(root());
        while (!q.isEmpty()) {
            node = q.remove();
            list.add(node);
            for (Node<E> child : children(node)) {
                q.add(child);
            }
        }
        return list;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
