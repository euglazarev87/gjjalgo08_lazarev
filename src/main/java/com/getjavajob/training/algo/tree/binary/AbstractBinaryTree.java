package main.java.com.getjavajob.training.algo.tree.binary;

import main.java.com.getjavajob.training.algo.tree.AbstractTree;
import main.java.com.getjavajob.training.algo.tree.Node;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {

    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parent = parent(n);
        Node<E> left = left(parent);
        if (left == null || left != n) {
            return left;
        } else {
            return right(parent);
        }
    }

    @Override
    public Iterable<Node<E>> children(Node<E> n) throws IllegalArgumentException {

        List<Node<E>> list = new ArrayList<>(2);
        Node<E> rc = right(n);
        Node<E> lc = left(n);
        if (lc != null) {
            list.add(lc);
        }
        if (rc != null) {
            list.add(rc);
        }

        return list;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {

        int count = 0;
        if (left(n) != null) {
            count++;
        }

        if (right(n) != null) {
            count++;
        }
        return count;
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Iterable<Node<E>> inOrder() {
        List<Node<E>> list = new ArrayList<>();
        inOrderTraversal(list, root());
        return list;
    }

    private void inOrderTraversal(List<Node<E>> list, Node<E> node) {
        if (node == null) {
            return;
        }
        inOrderTraversal(list, left(node));
        list.add(node);
        inOrderTraversal(list, right(node));
    }

    public int heightOfTree(Node<E> node) {
        return getTreeHeight(node);
    }

    public int heightOfTree() {
        return getTreeHeight(root());
    }

    protected int getTreeHeight(Node<E> node) {
        if (node == null) {
            return -1;
        }

        int lefth = getTreeHeight(left(node));
        int righth = getTreeHeight(right(node));

        if (lefth > righth) {
            return lefth + 1;
        } else {
            return righth + 1;
        }
    }

    @Override
    public String toString() {
        return "(" + root().toString() + ")";
    }
}
