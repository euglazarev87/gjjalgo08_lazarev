package main.java.com.getjavajob.training.algo.tree.binary;

import main.java.com.getjavajob.training.algo.tree.Node;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    protected int size;
    protected Node<E> root;

    public LinkedBinaryTree() {
        size = 0;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {

        if (n == null) {
            return null;
        }

        if (!(n instanceof NodeImpl)) {
            throw new IllegalArgumentException("Argument isn't compatible type");
        }

        return (NodeImpl) n;
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root != null) {
            throw new IllegalStateException("Root is not empty");
        }

        root = new NodeImpl<>(e);
        size++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        Node<E> newNode;

        if (right(node) == null) {
            newNode = addRight(node, e);
        } else {
            newNode = addLeft(node, e);
        }
        return newNode;
    }

    protected void checkNode(Node<E> node) throws IllegalArgumentException {
        for (Node<E> n : nodes()) {
            if (n == node) {
                return;
            }
        }
        throw new IllegalArgumentException("No such node " + node + " in tree");
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        checkNode(node);
        if (left(node) != null) {
            throw new IllegalArgumentException("Node left child isn't empty");
        }

        NodeImpl<E> newNode = new NodeImpl<>(e);
        node.left = newNode;
        size++;
        return newNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        checkNode(node);
        if (right(node) != null) {
            throw new IllegalArgumentException("Node right child isn't empty");
        }
        NodeImpl<E> newNode = new NodeImpl<>(e);
        node.right = newNode;
        size++;
        return newNode;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        checkNode(node);
        E oldValue = node.value;
        node.value = e;
        return oldValue;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            return null;
        }
        return validate(p).left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            return null;
        }
        return validate(p).right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return searchParent(root, validate(n));
    }

    protected Node<E> searchParent(Node<E> root, Node<E> n) {

        if (null == root) {
            return null;
        }

        Node<E> lh;
        Node<E> rh;
        if (left(root) == n || right(root) == n) {
            return root;
        }

        lh = searchParent(left(root), n);
        rh = searchParent(right(root), n);
        return lh != null ? lh : rh;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {

        protected E value;
        protected Node<E> left;
        protected Node<E> right;

        public NodeImpl(E v) {
            value = v;
        }

        @Override
        public E getElement() {
            return value;
        }

        @Override
        public String toString() {
            boolean hasChild = getLeft() != null || getRight() != null;
            return value + (hasChild ? " (" + left + ", " + right + ")" : "");
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Node<E> getLeft() {
            return left;
        }

        public void setLeft(Node<E> left) {
            this.left = left;
        }

        public Node<E> getRight() {
            return right;
        }

        public void setRight(Node<E> right) {
            this.right = right;
        }
    }
}