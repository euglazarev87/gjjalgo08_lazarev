package main.java.com.getjavajob.training.algo.tree.binary;

import main.java.com.getjavajob.training.algo.tree.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private List<Node<E>> list;
    private int size;

    public ArrayBinaryTree() {
        list = new ArrayList<>();
    }

    private int getIndexOf(Node<E> node) {
        int index = list.indexOf(node);
        if (index == -1) {
            throw new IllegalArgumentException("No such node " + node + " in tree");
        }
        return index;
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int index = getIndexForILeftChild(getIndexOf(p));
        if (index >= list.size()) {
            return null;
        }
        return list.get(index);
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int index = getIndexForRightChild(getIndexOf(p));
        if (index >= list.size()) {
            return null;
        }
        return list.get(index);
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        int leftIndex = getIndexForILeftChild(getIndexOf(n));
        if (list.get(leftIndex) != null) {
            throw new IllegalArgumentException("Left child of node " + n + "is not empty");
        }

        takeSpaceForChild();
        size++;
        Node<E> node = new NodeImpl<>(e);
        list.set(leftIndex, node);
        return node;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        int rightIndex = getIndexForRightChild(getIndexOf(n));
        if (list.get(rightIndex) != null) {
            throw new IllegalArgumentException("Left child of node " + n + "is not empty");
        }
        takeSpaceForChild();
        size++;
        Node<E> node = new NodeImpl<>(e);
        list.set(rightIndex, node);
        return node;
    }

    @Override
    public Node<E> root() {
        if (size > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = getIndexOf(n);
        int parentIndex = Math.abs((index - 1) / 2);
        return list.get(parentIndex);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (list.size() != 0) {
            throw new IllegalArgumentException("Root is not empty");
        }
        Node<E> root = new NodeImpl<>(e);
        size++;
        list.add(root);
        takeSpaceForChild();
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (right(n) == null) {
            return addRight(n, e);
        } else {
            return addLeft(n, e);
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        getIndexOf(n);
        NodeImpl<E> node = (NodeImpl<E>) n;
        E old = node.value;
        node.value = e;
        return old;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return preOrder();
    }

    private int getIndexForRightChild(int i) {
        return 2 * i + 2;
    }

    private int getIndexForILeftChild(int i) {
        return 2 * i + 1;
    }

    private void takeSpaceForChild() {
        list.add(null);
        list.add(null);
    }

    protected static class NodeImpl<E> implements Node<E> {

        E value;

        public NodeImpl(E v) {
            value = v;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}
