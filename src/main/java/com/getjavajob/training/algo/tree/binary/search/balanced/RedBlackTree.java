package main.java.com.getjavajob.training.algo.tree.binary.search.balanced;

import main.java.com.getjavajob.training.algo.tree.Node;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E extends Comparable<E>> extends BalanceableTree<E> {

    private static final boolean RED = false;
    private static final boolean BLACK = true;

    private static <E> boolean getColor(RBNode<E> node) {
        return null == node ? BLACK : node.color;
    }

    private boolean isBlack(Node<E> n) {
        if (n == null) {
            return true;
        }
        RBNode<E> node = validate(n);
        return getColor(node);
    }

    private boolean isRed(Node<E> n) {
        if (n == null) {
            return false;
        }
        RBNode<E> node = validate(n);
        return getColor(node) == RED;
    }

    private void makeBlack(Node<E> n) {
        if (n != null) {
            RBNode<E> node = validate(n);
            node.color = BLACK;
        }
    }

    private void makeRed(Node<E> n) {
        if (n != null) {
            RBNode<E> node = validate(n);
            node.color = RED;
        }
    }

    @Override
    public void remove(E e) {
        Node<E> p = getNode(e);
        if (p == null) {
            return;
        }

        size--;

        if (left(p) != null && right(p) != null) {
            Node<E> s = successor(p);
            validate(p).setValue(s.getElement());
            p = s;
        }

        Node<E> replacement = (left(p) != null ? left(p) : right(p));

        if (replacement != null) {

            if (parent(p) == null)
                root = replacement;
            else if (p == left(parent(p))) {
                validate(parent(p)).setLeft(replacement);
            } else {
                validate(parent(p)).setLeft(replacement);
            }

            validate(p).setLeft(null);
            validate(p).setRight(null);

            if (validate(p).color == BLACK) {
                afterElementRemoved(replacement);
            }
        } else if (parent(p) == null) {
            root = null;
        } else {
            if (validate(p).color == BLACK) {
                afterElementRemoved(p);
            }

            if (parent(p) != null) {
                if (p == left(parent(p))) {
                    validate(parent(p)).setLeft(null);
                } else if (p == right(parent(p))) {
                    validate(parent(p)).setRight(null);
                }
            }
        }

    }

    private Node<E> successor(Node<E> t) {
        if (t == null)
            return null;
        else if (right(t) != null) {
            Node<E> p = right(t);
            while (left(p) != null) {
                p = left(p);
            }
            return p;
        } else {
            Node<E> p = parent(t);
            Node<E> ch = t;
            while (p != null && ch == right(p)) {
                ch = p;
                p = parent(p);
            }
            return p;
        }
    }

    @Override
    public Node<E> add(E value) throws IllegalStateException {
        RBNode<E> newNode = new RBNode<>(value, RED);
        insert(validate(root()), newNode);
        afterElementAdded(newNode);
        size++;
        return newNode;
    }

    private void afterElementAdded(Node<E> n) {
        RBNode<E> node = validate(n);
        makeRed(node);

        while (node != null && node != root && isRed(parent(node))) {
            if (parent(node) == left(parent(parent(node)))) {
                RBNode<E> uncle = validate(right(parent(parent(node))));
                if (isRed(uncle)) {
                    makeBlack(parent(node));
                    makeBlack(uncle);
                    makeRed(parent(parent(node)));
                    node = validate(parent(parent(node)));
                } else {
                    if (node == right(parent(node))) {
                        node = validate(parent(node));
                        super.rotate(node);
                    }
                    makeBlack(parent(node));
                    makeRed(parent(parent(node)));
                    super.rotate(parent(node));
                }
            } else {
                RBNode<E> uncle = validate(left(parent(parent(node))));
                if (isRed(uncle)) {
                    makeBlack(parent(node));
                    makeBlack(uncle);
                    makeRed(parent(parent(node)));
                    node = validate(parent(parent(node)));
                } else {
                    if (node == left(parent(node))) {
                        node = validate(parent(node));
                        super.rotate(node);
                    }
                    makeBlack(parent(node));
                    makeRed(parent(parent(node)));
                    super.rotate(parent(node));
                }
            }
        }
        RBNode<E> r = validate(root());
        r.color = BLACK;
    }

    private void afterElementRemoved(Node<E> n) {

        while (n != root && isBlack(n)) {
            if (n == left(parent(n))) {
                Node<E> sib = right(parent(n));

                if (isRed(sib)) {
                    makeBlack(sib);
                    makeRed(parent(n));
                    super.rotate(parent(n));
                    sib = right(parent(n));
                }

                if (isBlack(left(sib)) && isBlack(right(sib))) {
                    makeRed(sib);
                    n = parent(n);
                } else {
                    if (isBlack(right(sib))) {
                        makeBlack(left(sib));
                        makeRed(sib);
                        super.rotate(sib);
                        sib = right(parent(n));
                    }
                    if (isBlack(parent(n))) {
                        makeBlack(sib);
                    } else {
                        makeRed(sib);
                    }

                    makeBlack(parent(n));
                    makeBlack(right(sib));
                    super.rotate(parent(n));
                    n = root;
                }
            } else {
                Node<E> sib = left(parent(n));

                if (isRed(sib)) {
                    makeBlack(sib);
                    makeRed(parent(n));
                    super.rotate(parent(n));
                    sib = left(parent(n));
                }

                if (isBlack(right(sib)) && isBlack(left(sib))) {
                    makeRed(sib);
                    n = parent(n);
                } else {
                    if (isBlack(left(sib))) {
                        makeBlack(right(sib));
                        makeRed(sib);
                        super.rotate(sib);
                        sib = left(parent(n));
                    }
                    if (isBlack(parent(n))) {
                        makeBlack(sib);
                    } else {
                        makeRed(sib);
                    }

                    makeBlack(parent(n));
                    makeBlack(left(sib));
                    super.rotate(parent(n));
                    n = root;
                }
            }
        }
        makeBlack(n);
    }

    protected RBNode<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (!(n instanceof RBNode)) {
            throw new IllegalArgumentException("Not compatible type");
        }
        return (RBNode<E>) n;
    }

    protected static class RBNode<V> extends NodeImpl<V> {

        boolean color;

        public RBNode(V value, boolean color) {
            super(value);
            this.color = color;
        }
    }
}
